
-- Townhall

function Initialise()

    SetData("TurnTime", 90000)
    SetData("RoundTime", 3000000)
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 0          
    CloseContainer(lock)
    
    SendMessage("GameLogic.PlaceObjects")

    lib_SetupTeam(0, "Player_Team")
    lib_SetupTeam(1, "CPU_Team")

	lib_SetupWorm(0, "Player_Worm1")
    lib_SetupWorm(1, "Player_Worm2")
    lib_SetupWorm(2, "Player_Worm3")
    lib_SetupWorm(3, "Player_Worm4")
    lib_SetupWorm(4, "CPU_Worm1")
    lib_SetupWorm(5, "CPU_Worm2")
    lib_SetupWorm(6, "CPU_Worm3")
    lib_SetupWorm(7, "CPU_Worm4")
    lib_SetupWorm(8, "CPU_Worm5")
    lib_SetupWorm(9, "CPU_Worm6")
    
    WormAILevel = "AIParams.CPU1"
    CopyContainer(WormAILevel, "AIParams.Worm04")
    CopyContainer(WormAILevel, "AIParams.Worm05")
    CopyContainer(WormAILevel, "AIParams.Worm06")
    CopyContainer(WormAILevel, "AIParams.Worm07")
    CopyContainer(WormAILevel, "AIParams.Worm08")
    CopyContainer(WormAILevel, "AIParams.Worm09")    
    
    SendMessage("WormManager.Reinitialise")

    lib_SetupTeamInventory(1, "CPU_Inventory")
    lib_SetupTeamInventory(0, "Player_Inventory")
    
    SetData("Trigger.Visibility", 0)
    lib_SpawnTrigger("Trigger1")
    lib_SpawnTrigger("Trigger2")
    lib_SpawnTrigger("Trigger3")
    lib_SpawnTrigger("Trigger4")
    --lib_SpawnTrigger("Trigger5")
    lib_SpawnTrigger("Trigger6")
    lib_SpawnTrigger("Trigger7")
    lib_SpawnTrigger("Trigger8")
    lib_SpawnTrigger("Trigger9")
    lib_SpawnTrigger("Trigger10")
     
    lib_SpawnCrate("Targ1")
    lib_SpawnCrate("Targ2")
    lib_SpawnCrate("Targ3")
    lib_SpawnCrate("Targ4")
    lib_SpawnCrate("Targ5")
    lib_SpawnCrate("Targ6")
     
    GameHasEnded = false
    Land = 100
    DeadWormNumber = 0
    PercentRemaining = 100
    BarrelsDestroyed = 0
    BuildingHasBeenDestroyed = false
    BuildingMovieHasPlayed = false
    MovieHasPlayed = false
    Dontgoinhereagain = false
    MovieHasPlayedSoDontStartTurnAgain = false
    local EasterEgg= QueryContainer( "Lock.EasterEgg.0")
    if EasterEgg.State == "Unlocked" then
        Dontgoinhereagain = true
    end
    
   -- SetData("HUD.Counter.Active",1)
    --SetData("HUD.Counter.Value", Land)
    --SetData("HUD.Counter.Percent",1)
    --Emitter1 = lib_CreateEmitter("WXPL_BTF_Fire", "BTF_FIRE")
    PlayIntroMovie()
     
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Destruct and serve intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Destruct and serve intro" then
    
        StartFirstTurn()
        
    elseif WhichMovie == "NowKillWorms" or WhichMovie == "NowDestroyBuilding" or WhichMovie == "EasterMovie" then
    
        StartTurn()
        
    end
    
end


function Worm_Died()

   local deadworm = GetData("DeadWorm.Id")
   local Team = lib_GetWormTeamIndex(deadworm)
   
    if Team == 1 then

        DeadWormNumber = DeadWormNumber + 1   
        local Crate1 = {"SuperSheep_Crate1", "Banana_Crate1", 
                        "Holy_Crate1", "Airstrike_Crate1","Airstrike_Crate2","Airstrike_Crate3"}
                        
        if DeadWormNumber == 6 and BuildingHasBeenDestroyed == true then             
            
        else
            lib_SpawnCrate(Crate1[DeadWormNumber])
        end
        
    end
    

end



--Success/Failure checks

function TurnEnded()


    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
             GameHasEnded = true
            --SetData("GameToFrontEndDelayTime",1000)
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        
        else

            TeamCount = lib_GetActiveAlliances()
            WhichTeam = lib_GetSurvivingTeamIndex()
    
            if TeamCount == 0 then
                GameHasEnded = true
                SendMessage("GameLogic.Mission.Failure")
                lib_DisplayFailureComment()
            
            elseif TeamCount == 1 and WhichTeam == 1 then
                GameHasEnded = true
                SendMessage("GameLogic.Mission.Failure")
                lib_DisplayFailureComment()
        
            elseif TeamCount == 1 and WhichTeam == 0 and BuildingHasBeenDestroyed == true then
    
                GameHasEnded = true
        
                lib_SetupWorm(10, "OutroWorm1")  -- EFMV outro worms
                SendIntMessage("Worm.Respawn", 10)  
                
                lib_SetupWorm(11, "OutroWorm2") 
                SendIntMessage("Worm.Respawn", 11) 
                
                lib_SetupWorm(12, "OutroWorm3") 
                SendIntMessage("Worm.Respawn", 12) 
                
                lib_SetupWorm(13, "OutroWorm4") 
                SendIntMessage("Worm.Respawn", 13) 
                
                lib_SetupWorm(14, "OutroWorm5") 
                SendIntMessage("Worm.Respawn", 14)  
                
                lib_SetupWorm(15, "OutroWorm6") 
                SendIntMessage("Worm.Respawn", 15)   
    
    
                SetData("EFMV.GameOverMovie","Outro")
                SetData("WXD.StoryMovie", "Camelot")
                SendMessage("GameLogic.Mission.Success")
        
            else
    
            if BuildingHasBeenDestroyed == true  then -- Play this movie if the building has been destroyed
   

                if DeadWormNumber < 6 and BuildingMovieHasPlayed == false then
        
                    SetData("EFMV.MovieName", "NowKillWorms")
                    SendMessage("EFMV.Play")
            
                    BuildingMovieHasPlayed = true
                    MovieHasPlayedSoDontStartTurnAgain = true
            
                end
            end
   
            if DeadWormNumber == 6 and BuildingHasBeenDestroyed == false then -- Play this movie if the worms are dead
    
                SetData("EFMV.MovieName", "NowDestroyBuilding")
                SendMessage("EFMV.Play")
                MovieHasPlayedSoDontStartTurnAgain = true
                DeadWormNumber = 0
            end
   
        end
    
        if GameHasEnded == false and MovieHasPlayedSoDontStartTurnAgain == false then -- Fudge fix to stop it trying to start a new turn when the outro is playing.
    
            StartTurn()
        
        end
        
    end
    
    
end

function TurnStarted() 

    MovieHasPlayedSoDontStartTurnAgain = false
    
end


function Trigger_Destroyed()

    TriggerIndex = GetData("Trigger.Index")
    
    if TriggerIndex < 8 then
    
        BarrelsDestroyed = BarrelsDestroyed + 1
        
            if BarrelsDestroyed == 6 then
    
                BuildingHasBeenDestroyed = true
        
            end

        local Doubledamage = GetData("DoubleDamage")
        SetData("DoubleDamage", 0) -- Stops the double damage from destroying all the town hall
        local ExplosionSite = {"Explosion1","Explosion2","Explosion3","Explosion4","Explosion5","Explosion6","Explosion7"}

        lib_CreateExplosion(ExplosionSite[TriggerIndex], 100, 0.35, 120, 120, 120)
    
        SetData("DoubleDamage", Doubledamage)
        
    
    elseif TriggerIndex == 8 and Dontgoinhereagain == false then -- Car has been blownup
    
        Dontgoinhereagain = true
    
        SetData("EFMV.MovieName", "EasterMovie")
        SendMessage("EFMV.Play")
        --lib_CreateEmitter("WXPL_BTF_Fire", "TIRE_FIRE")
        StartTimer("LetThereBeFire",1600) 
        SendStringMessage("WXMsg.EasterEggFound", "Lock.EasterEgg.0") 
    
    end
    
  
end


function LetThereBeFire() -- I would play this within the EFMV but it don't seem to work....

    lib_CreateEmitter("WXPL_BTF_Fire", "TIRE_FIRE")

end


--function Land_NewShape() -- Runs whenever a bit of land has been destroyed.


--        SendStringMessage("Land.GetLandRemaining", "AAAA")
--        local LandRemaining = GetData("Land.LandRemaining")

--    if BuildingHasBeenDestroyed == false then
--    
--        if LandRemaining == 3243 then 
--        
--            return(0)
--            
--        else

--           SendStringMessage("Land.GetLandRemaining", "AAAA")
--           local LandRemaining = GetData("Land.LandRemaining")
--           local MinBlocks = 1000
--           local MaxBlocks = 3141
--           LandRemaining = LandRemaining - 500
--           PercentRemaining = 100*(LandRemaining-MinBlocks)/(MaxBlocks-MinBlocks)
--       
--    
--   
--            if PercentRemaining < 1 then
--            PercentRemaining = 0 
--            end
--    
--            if PercentRemaining == 0 then
--    
--            BuildingHasBeenDestroyed = true
--            end
--   
--    
--            SetData("HUD.Counter.Value", PercentRemaining)
--    
--        end
--        
--    end
--       
--end




