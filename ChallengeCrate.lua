function Initialise()
	
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
    SetData("RoundTime",0)
    SetData("HUD.Clock.DisplayTenths", 1)
	SetData("HotSeatTime", 0) 
    SetData("RetreatTime", 0) 
	
	lib_SetupTeam(0, "PlayerTeam")
	lib_SetupWorm(0, "PlayerWorm")
	lib_SetupTeamInventory(0, "PlayerInventory")
	
	
	SendMessage("WormManager.Reinitialise")
    SendMessage("Commentary.NoDefault")
	SetData("Trigger.Visibility", 1)

    g_nNextTarget = 1
    
    lib_SpawnCrate("Crate1") 
    lib_SpawnCrate("Crate2") 
    lib_SpawnCrate("Crate3") 
    lib_SpawnCrate("Crate4") 
    lib_SpawnCrate("Crate5") 
    lib_SpawnCrate("Crate6") 
    lib_SpawnCrate("Crate7") 
    lib_SpawnCrate("Crate8") 
    lib_SpawnCrate("Crate9") 
    lib_SpawnCrate("Crate10") 
    TargetsLeft = 10
    --SpawnNextTarget()
    SetHudCounter()
   -- StartFirstTurn()
   PlayIntroMovie()
   
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    StartFirstTurn()
end

function Crate_Collected()

    TargetsLeft = TargetsLeft - 1
    SetData("HUD.Counter.Value", TargetsLeft)
    
    TextToDisplay = {"C.Generic.Good.1","C.Generic.Good.2","C.Generic.Good.3","C.Generic.Time.1"}
    local myRandomInteger = lib_GetRandom(1, 4)
        
    lib_Comment(TextToDisplay[myRandomInteger])
   
  if TargetsLeft == 0 then
 
    lib_DisplaySuccessComment()
    SetData("EFMV.GameOverMovie", "Outro")
    SendMessage("GameLogic.Challenge.Success")
    
    end

   -- SpawnNextTarget()

end

function SetHudCounter()
    
    SetData("HUD.Counter.Active", 1) 
    SetData("HUD.Counter.Value", TargetsLeft) 
end



function Crate_Sunk() -- Just in case

    IndexNumber = GetData("Crate.Index")
    
    local Crate = {"Crate1","Crate2","Crate3","Crate4","Crate5","Crate6","Crate7","Crate8","Crate9","Crate10"}
    lib_SpawnCrate(Crate[IndexNumber])
    
end


--function SpawnNextTarget()

--    if g_nNextTarget == 16 then
--    
--        lib_DisplaySuccessComment(
--        SetData("EFMV.GameOverMovie", "Outro")
--        SendMessage("GameLogic.Challenge.Success")
--        
--    else
--    
--    -- target cycle 
--    local Crate = {"Targ1", "Targ2", "Targ3", "Targ4", "Targ5", "Targ6", 
--    				"Targ7", "Targ8", "Targ9", "Targ10","Targ11", "Targ12", "Targ13", "Targ14", "Targ15"}
--        lib_SpawnCrate(Crate[g_nNextTarget])
--        g_nNextTarget = g_nNextTarget + 1
--        
--    log("g_nNextTarget = ", g_nNextTarget)
--    
--    end

--end


function TurnEnded()
        StartTurn()
    
end

function Worm_Died()
    
    deadworm = GetData("DeadWorm.Id")
    
    if deadworm == 0 then
    
        lib_DisplayFailureComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Failure")
    end
end


