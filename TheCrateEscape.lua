function Initialise()
    
    SetData("Land.Indestructable", 1)
--~     SetData("TurnTime", 45000)

    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 0          
    CloseContainer(lock)

    lib_SetupTeam(0, "HumanTeam")
	lib_SetupTeam(1, "BaddieTeam")
	
	lib_SetupWorm(0, "Player")
    lib_SetupWorm(1, "Player2")
    
    lib_SetupWorm(2, "baddie1")
    lib_SetupWorm(3, "baddie2")
    lib_SetupWorm(4, "baddie3")
    lib_SetupWorm(5, "baddie4")

    WormAILevel = "AIParams.CPU1"
    CopyContainer(WormAILevel, "AIParams.Worm02")
    CopyContainer(WormAILevel, "AIParams.Worm03")
    CopyContainer(WormAILevel, "AIParams.Worm04")
    CopyContainer(WormAILevel, "AIParams.Worm05")
    
    SendMessage("WormManager.Reinitialise")
    
    lib_SetupTeamInventory(0, "HumanWeapz")
    lib_SetupTeamInventory(1, "BaddieWeapz")
    lib_SpawnCrate("crate")
    lib_SpawnCrate("crate2")

    
    lib_SpawnTrigger("Trig0")
    lib_SpawnTrigger("Trig1")
    
    
    PlayCrateHintMovie = false
    HumanDead = 0
    CPUDead = 0
    EFMV_Played_Once = false
        
	SetData("EFMV.MovieName", "Intro")
	SendMessage("EFMV.Play") 
    

    Emitter = lib_CreateEmitter("WXP_CollectableItem", "Party1") 
    
end

function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then
    
        StartFirstTurn()
        
    elseif WhichMovie == "CrateHint" then
    
        EFMV_Played_Once = true
        
    end
end

function TurnEnded()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
        SetData("GameToFrontEndDelayTime",1000)
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    elseif HumanDead == 2 then
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
            
          
    else
        StartTurn()
    end
    
end


function Crate_Collected()
        
    CrateIndex = GetData("Crate.Index")
        
    if CrateIndex == 0 then
--        
        lib_Comment("M.Con.Crate.CON.1")
        SetData("EFMV.MovieName", "Midtro")
        SendMessage("EFMV.Play") 
    
    elseif CrateIndex == 1 then--and BaddieDead == 4 then
       
        lib_DeleteEmitterImmediate(Emitter)
        lib_Comment("M.Con.Crate.CON.2")
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success")
        
        lib_DisplaySuccessComment()

    end
end


function Worm_Died()

    DeadWorm = GetData("DeadWorm.Id")
    
    if DeadWorm == 0 or DeadWorm == 1 then 
        
        HumanDead = HumanDead + 1
        
    elseif DeadWorm > 1 then
    
        CPUDead = CPUDead + 1
        
    end
    
    if CPUDead == 4 and EFMV_Played_Once == false then
            
        PlayCrateHintMovie = true 
    end
end

function Trigger_Collected()
    SendMessage("Commentary.NoDefault")
end


function DoOncePerTurnFunctions()

    if PlayCrateHintMovie == true then
    
        SetData("EFMV.MovieName", "CrateHint")
        SendMessage("EFMV.Play") 
    
    end
    
    
    PlayCrateHintMovie = false
end
    
    
    