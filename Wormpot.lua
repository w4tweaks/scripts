-------------------------------------------------------------------------------
-- Script to apply the wormpot modes
-------------------------------------------------------------------------------

-----------------------------------------------
function EndEarthquake()
	SendMessage("Earthquake.End")
end


function DoWormpotOncePerTurnFunctions()
	-- Set off any end-of-turn wormpot settings 
	local Wormpot = QueryContainer("WormPot")
	
    -----------------------------------------------------------
	if (Wormpot.CratesOnly == true) or (Wormpot.LotsOfCrates == true) then
		--Print("SendMessage(GameLogic.CrateShower)")
		SendMessage("GameLogic.CrateShower")
	end
		

    -----------------------------------------------------------
    -- Double damage
	if Wormpot.DoubleDamage == true then
--			WaitUntilNoActivity = true;		
--			WaitingForWormpot = true
		
		SetData("DoubleDamage",1)
--			SendMessage("DoubleDamage.Activated")
	end
end

-----------------------------------------------------------------------------

function SetWormpotModes()

    SendMessage("Wormpot.SetupModes")
    
	local WormpotLock, Wormpot 
	local ContainerLock, Container
	local DamageScale, PowerScale

	-- initialise wormpot global vars
	WaitingForWormpot = false
	
	local Wormpot = QueryContainer("WormPot")
	
	DamageScale = Wormpot.SuperScale
	PowerScale = Wormpot.PowerScale

   	--------------------------------------------------------------------
	-- Super Firearms
	if Wormpot.SuperFirearms == true then
		ApplyWormpotDamageScale("kWeaponShotgun",DamageScale)
		ApplyWormpotDamageScale("kWeaponSniperRifle",DamageScale)

		ApplyWormpotPowerScale("kWeaponShotgun",PowerScale)
		ApplyWormpotPowerScale("kWeaponSniperRifle",PowerScale)
	end
   
   --------------------------------------------------------------------
	-- Super hand to hand weapons
	if Wormpot.SuperHand == true then
		ApplyWormpotDamageScale("kWeaponBaseballBat",DamageScale)
		ApplyWormpotDamageScale("kWeaponProd",DamageScale)
		ApplyWormpotDamageScale("kWeaponFirePunch",DamageScale)
		ApplyWormpotDamageScale("kWeaponNoMoreNails",DamageScale)

		ApplyWormpotPowerScale("kWeaponBaseballBat",PowerScale)
		ApplyWormpotPowerScale("kWeaponProd",PowerScale)
		ApplyWormpotPowerScale("kWeaponFirePunch",PowerScale)
		ApplyWormpotPowerScale("kWeaponNoMoreNails",PowerScale)
	end

   --------------------------------------------------------------------
	-- Super cluster weapons
	if Wormpot.SuperClusters == true then
		ApplyWormpotDamageScale("kWeaponClusterGrenade",DamageScale)
		ApplyWormpotDamageScale("kWeaponClusterBomb",DamageScale)
		ApplyWormpotDamageScale("kWeaponAirstrike",DamageScale)
		ApplyWormpotDamageScale("kWeaponSuperAirstrike",DamageScale)
		ApplyWormpotDamageScale("kWeaponBananaBomb",DamageScale)
		ApplyWormpotDamageScale("kWeaponBananette",DamageScale)

		ApplyWormpotPowerScale("kWeaponClusterGrenade",PowerScale)
		ApplyWormpotPowerScale("kWeaponClusterBomb",PowerScale)
		ApplyWormpotPowerScale("kWeaponAirstrike",PowerScale)
		ApplyWormpotPowerScale("kWeaponSuperAirstrike",PowerScale)
		ApplyWormpotPowerScale("kWeaponBananaBomb",PowerScale)
		ApplyWormpotPowerScale("kWeaponBananette",PowerScale)
	end

	--------------------------------------------------------------------
	-- Super animal weapons
	if Wormpot.SuperAnimals == true then
		ApplyWormpotDamageScale("kWeaponSheep",DamageScale)
		ApplyWormpotDamageScale("kWeaponSuperSheep",DamageScale)
		ApplyWormpotDamageScale("kWeaponOldWoman",DamageScale)
		ApplyWormpotDamageScale("kWeaponConcreteDonkey",DamageScale)
		ApplyWormpotDamageScale("kWeaponScouser",DamageScale)

		ApplyWormpotPowerScale("kWeaponSheep",PowerScale)
		ApplyWormpotPowerScale("kWeaponSuperSheep",PowerScale)
		ApplyWormpotPowerScale("kWeaponOldWoman",PowerScale)
		ApplyWormpotPowerScale("kWeaponConcreteDonkey",PowerScale)
		ApplyWormpotPowerScale("kWeaponScouser",PowerScale)
	end

	--------------------------------------------------------------------
	-- Super explosive weapons
	if Wormpot.SuperExplosives == true then
		ApplyWormpotDamageScale("kWeaponBazooka",DamageScale)
		ApplyWormpotDamageScale("kWeaponDynamite",DamageScale)
		ApplyWormpotDamageScale("kWeaponGrenade",DamageScale)
		ApplyWormpotDamageScale("kWeaponHolyHandGrenade",DamageScale)
		ApplyWormpotDamageScale("kWeaponLandmine",DamageScale)
		ApplyWormpotDamageScale("kWeaponHomingMissile",DamageScale)
		ApplyWormpotDamageScale("kWeaponGasCanister",DamageScale)
		ApplyWormpotDamageScale("kWeaponFatkins",DamageScale)

		ApplyWormpotPowerScale("kWeaponBazooka",PowerScale)
		ApplyWormpotPowerScale("kWeaponDynamite",PowerScale)
		ApplyWormpotPowerScale("kWeaponGrenade",PowerScale)
		ApplyWormpotPowerScale("kWeaponHolyHandGrenade",PowerScale)
		ApplyWormpotPowerScale("kWeaponLandmine",PowerScale)
		ApplyWormpotPowerScale("kWeaponHomingMissile",PowerScale)
		ApplyWormpotPowerScale("kWeaponGasCanister",PowerScale)
		ApplyWormpotPowerScale("kWeaponFatkins",PowerScale)
	end
    
    -- Weapons not in the SuperWeapons :
    --
    --      kWeaponStarburst,
    --      kWeaponFactoryWeapon,
    --      kWeaponAlienAbduction,
    --      kWeaponFlood,
    

   --------------------------------------------------------------------
   -- One shot one kill (i.e set health to 1!)
	if  Wormpot.OneShotOneKill then
		local WormIndex = 0
		local WormContainerName = lib_GetWormContainerName(WormIndex)
		
		while WormContainerName ~= "" do
		
			local lock, worm = EditContainer(WormContainerName)
			worm.Energy = 1
			CloseContainer(lock)
			
			WormIndex = WormIndex + 1
			WormContainerName = lib_GetWormContainerName(WormIndex)
		end
	end

	-----------------------------------------------------------
	if Wormpot.CratesOnly == true then
		SendMessage("GameLogic.ClearInventories")
		SendMessage("GameLogic.CrateShower")
	end
	
	-----------------------------------------------------------
	if Wormpot.LotsOfCrates == true then
		SendMessage("GameLogic.CrateShower")
	end
   --------------------------------------------------------------------
   -- Double Damage
	if Wormpot.DoubleDamage == true then
		SetData("DoubleDamage",1)
	end
	
	-----------------------------------------------------------
	-- Falling really hurts
	if Wormpot.FallingHurtsMore == true then
		fDamage = GetData("Worm.FallDamageRatio")
		fDamage = fDamage * Wormpot.FallingScale
		SetData("Worm.FallDamageRatio",fDamage)
	end
  	
  	-----------------------------------------------------------
  	-- No retreat time
  	if Wormpot.NoRetreatTime == true then 
        -- Set the retreat time to zero
  		SetData("DefaultRetreatTime",0)
  		SetData("RetreatTime",0)
  		
  		-- Now remove the surrender from the inventories

  		-- Team inventory	
  		local Index = 0
  		local InventoryName = lib_GetTeamInventoryName(Index)
  	
  		while InventoryName ~= "" do
  		
  			local Lock,Container = EditContainer(InventoryName)
  			Container.Surrender = 0
  			CloseContainer(Lock)
  			
  			Index = Index + 1  			
  			InventoryName = lib_GetTeamInventoryName(Index)
  		end
  		
  		-- Alliance inventory
  		Index = 0
  		InventoryName = lib_GetAllianceInventoryName(Index)
  		
  		while InventoryName ~= "" do
  		
  			local Lock,Container = EditContainer(InventoryName)
  			Container.Surrender = 0
  			CloseContainer(Lock)
  			
  			Index = Index + 1  			
	  		InventoryName = lib_GetAllianceInventoryName(Index)
  		end
  		
  		-- Worms
  		Index = 0
  		InventoryName = lib_GetWormInventoryName(Index)
  		
  		while InventoryName ~= "" do
  		
  			local Lock,Container = EditContainer(InventoryName)
  			Container.Surrender = 0
  			CloseContainer(Lock)
  			
  			Index = Index + 1  			
	  		InventoryName = lib_GetWormInventoryName(Index)
  		end
  		
  	end
  			
	-----------------------------------------------------------
	-- Energy or enemy
	if Wormpot.EnergyOrEnemy == true then
		local WormIndex = 0
		local WormContainerName = lib_GetWormContainerName(WormIndex)
		local PoisonRate = GetData("Worm.Poison.Default")
		
		-- Poison each worm
		while WormContainerName ~= "" do
		
			local lock, worm = EditContainer(WormContainerName)
			worm.PoisonRate = PoisonRate
			CloseContainer(lock)
			
			SendIntMessage("Worm.Poison",WormIndex);
			
			WormIndex = WormIndex + 1
			WormContainerName = lib_GetWormContainerName(WormIndex)
		end
	end
	
	-----------------------------------------------------------
	-- Slippy mode
	if Wormpot.SlippyMode == true then
      SetStickySlippyMode(Wormpot.SlippyModeScale)
	end
	
	-----------------------------------------------------------
	-- Sticky mode
	if Wormpot.StickyMode == true then
	-- Note - Sticky mode scale is a value between 0 & 1. Multiplying it by 20
	-- gives a 'better' result here ie is an arbitrary value that works ok!
      SetStickySlippyMode(Wormpot.StickyModeScale * 20)
	end
	
	-----------------------------------------------------------
	-- Wind effects more weapons
	if Wormpot.WindEffectMore == true then
		SetWeaponWind("kWeaponAirstrike",true)
		SetWeaponWind("kWeaponBananaBomb",true)
		SetWeaponWind("kWeaponBananette",true)
		SetWeaponWind("kWeaponClusterBomb",true)
		SetWeaponWind("kWeaponClusterGrenade",true)
		SetWeaponWind("kWeaponConcreteDonkey",true)
		SetWeaponWind("kWeaponGasCanister",true)
		SetWeaponWind("kWeaponGrenade",true)
		SetWeaponWind("kWeaponHolyHandGrenade",true)
		SetWeaponWind("kWeaponOldWoman",true)
		SetWeaponWind("kWeaponSheep",true)
		SetWeaponWind("kWeaponScouser",true)
		SetWeaponWind("kWeaponFatkins",true)
		SetWeaponWind("kWeaponPoisonArrow",true)
		SetWeaponWind("kWeaponFactoryWeapon",true)
		SetWeaponWind("kWeaponDynamite",true)
		SetWeaponWind("kWeaponLandmine",true)
		SetWeaponWind("kWeaponSuperAirstrike",true)
	end
	
	-----------------------------------------------------------
	-- David and goliath - One worm has lots of energy & the rest have little energy

	if ( Wormpot.DavidAndGoliath == true ) then	
		local GM = QueryContainer("GM.GameInitData")
		local LargestTeam = GM.T1_NumOfWorms
		
		-- First. Find the largest number of worms in one team
	   if GM.NumberOfTeams > 1 then 
			if GM.T2_NumOfWorms > LargestTeam then
				LargestTeam = GM.T2_NumOfWorms
			end
		end
		
	   if GM.NumberOfTeams > 2 then 
			if GM.T3_NumOfWorms > LargestTeam then
				LargestTeam = GM.T3_NumOfWorms
			end
		end
		
	   if GM.NumberOfTeams > 3 then 
			if GM.T4_NumOfWorms > LargestTeam then
				LargestTeam = GM.T4_NumOfWorms
			end
		end
		
	   if GM.NumberOfTeams > 4 then 
			if GM.T5_NumOfWorms > LargestTeam then
				LargestTeam = GM.T5_NumOfWorms
			end
		end
		
	   if GM.NumberOfTeams > 5 then 
			if GM.T6_NumOfWorms > LargestTeam then
				LargestTeam = GM.T6_NumOfWorms
			end
		end
		
		LargestTeam = LargestTeam * 100
		
		if GM.NumberOfTeams >= 1 then
			SetDavidAndGolithHealth(LargestTeam,GM.T1_NumOfWorms,0)
		end 
			
		if GM.NumberOfTeams >= 2 then
			SetDavidAndGolithHealth(LargestTeam,GM.T2_NumOfWorms,1)
		end 
			
		if GM.NumberOfTeams >= 3 then
			SetDavidAndGolithHealth(LargestTeam,GM.T3_NumOfWorms,2)
		end 
	   
		if GM.NumberOfTeams >= 4 then
			SetDavidAndGolithHealth(LargestTeam,GM.T4_NumOfWorms,3)
		end 
		
		if GM.NumberOfTeams >= 5 then
			SetDavidAndGolithHealth(LargestTeam,GM.T5_NumOfWorms,4)
		end 
		
		if GM.NumberOfTeams >= 6 then
			SetDavidAndGolithHealth(LargestTeam,GM.T6_NumOfWorms,5)
		end 
	end
	
	-----------------------------------------------------------
	if Wormpot.SpecialistWorms == true then
		local GM = QueryContainer("GM.GameInitData")
		
		if GM.NumberOfTeams >= 1 then
			SetSpecialistTeam(GM.T1_NumOfWorms,0)
		end
		
		if GM.NumberOfTeams >= 2 then
			SetSpecialistTeam(GM.T2_NumOfWorms,1)
		end
			
		if GM.NumberOfTeams >= 3 then
			SetSpecialistTeam(GM.T3_NumOfWorms,2)
		end
			
		if GM.NumberOfTeams >= 4 then
			SetSpecialistTeam(GM.T4_NumOfWorms,3)
		end
			
		if GM.NumberOfTeams >= 5 then
			SetSpecialistTeam(GM.T5_NumOfWorms,4)
		end
			
		if GM.NumberOfTeams >= 6 then
			SetSpecialistTeam(GM.T6_NumOfWorms,5)
		end
	end
end

-- The worm types are 1=CANNONADE_GRENADIER, 2=MELEE_ENGINEER, 3=MELEE, 4=ENGINEER, 5=CANNONADE, 6=GRENADIER 
-- The worm type table is really a two dimensional array of TotalNumOfWormsInTeam X WormNumber
------------------------------------------------------------------------
function SetSpecialistTeam(NumOfWorms, LocalTeamIndex)

	local WormIndex = 0
	local WormContainerName = lib_GetWormContainerName(WormIndex)
	local CurrentWorm = 1
	local SpecialistWormTypeTable = { 0,1,1,5,5,5, 0,2,3,6,6,6, 0,0,4,3,3,3, 0,0,0,4,4,4, 0,0,0,0,6,6, 0,0,0,0,0,3 }
	local Index = 0
	local SpecialistWormType = 0
			
	if NumOfWorms > 1 then
		while WormContainerName ~= "" do
			local WormLock, WormContainer = EditContainer(WormContainerName)
										
			if WormContainer.TeamIndex == LocalTeamIndex then

				local InventoryLock, Inventory = EditContainer(lib_GetTeamInventoryName(LocalTeamIndex))
				
				index = ((CurrentWorm - 1)  * 6 ) + NumOfWorms
				SpecialistWormType = SpecialistWormTypeTable[index]
								
				if SpecialistWormType > 0 then
				
					-- Firstly disable all this worms weapons
					DisallowAllWeapons(WormContainer)
				
					-- Now enable the appropriate weapons for the worm type
					
					
					if SpecialistWormType == 3 or SpecialistWormType == 2 then
						-- Melee
						WormContainer.AllowShotgun = 1         
						WormContainer.AllowAirstrike = 1       
						WormContainer.AllowLandmine = 1        
						WormContainer.AllowFirePunch = 1      
						WormContainer.AllowProd = 1       
    
						Inventory.Shotgun = -1         
						Inventory.Airstrike = 1       
						Inventory.Landmine = 2        
						Inventory.FirePunch = -1     
						Inventory.Prod = -1      
					end
					
					if SpecialistWormType == 4 or SpecialistWormType == 2 then
						-- Engineer
						WormContainer.AllowNinjaRope = 1 
						WormContainer.AllowGirder = 1       
						WormContainer.AllowDynamite = 1
						WormContainer.AllowParachute = 1 
						WormContainer.AllowBaseballBat = 1
						WormContainer.AllowSheep = 1
						WormContainer.AllowTeleport = 1
						
						Inventory.NinjaRope = 5 
						Inventory.Girder = 3       
						Inventory.Dynamite = 1
						Inventory.Parachute = 2 
						Inventory.BaseballBat = 1
						Inventory.Sheep = 1
						Inventory.Teleport = 2
					end
					
					if SpecialistWormType == 5 or SpecialistWormType == 1 then
						-- CANNONADE
						WormContainer.AllowBazooka = 1
						WormContainer.AllowHomingMissile = 1
						Inventory.Bazooka = -1
						Inventory.HomingMissile = 1
					end
					
					if SpecialistWormType == 6 or SpecialistWormType == 1 then
						-- Grenadier
						WormContainer.AllowGrenade = 1
						WormContainer.AllowClusterGrenade = 1
						Inventory.Grenade = -1
						Inventory.ClusterGrenade = 3
					end

					
				end
				
				CloseContainer(InventoryLock)
				
				CurrentWorm = CurrentWorm + 1
				
				if CurrentWorm > NumOfWorms then
					CloseContainer(WormLock)
					
					local DelayLock, DelayContainer = EditContainer("Inventory.WeaponDelays.Default")
					DelayContainer.HomingMissile = 1
					DelayContainer.Airstrike = 5
					CloseContainer(DelayLock)

                    -- Setup Default Weapon Delays
					
                    local index
                    for index = 0,3 do
                       lib_SetupTeamWeaponDelays(index, "Inventory.WeaponDelays.Default")
                    end
                   
					break
				end
					
			end
					
			CloseContainer(WormLock)
			
			WormIndex = WormIndex + 1
			WormContainerName = lib_GetWormContainerName(WormIndex)
		end
	end
	
end

------------------------------------------------------------------------
function SetDavidAndGolithHealth(LargestTeam, NumOfWorms, LocalTeamIndex)

	local SmallHealth = LargestTeam / ( NumOfWorms * 2 );
	local BigHealth = LargestTeam - ( (NumOfWorms - 1) * SmallHealth);
	local WormIndex = 0
	local SetBigHealth = true
	local WormContainerName = lib_GetWormContainerName(WormIndex)
	
	-- Set the health of each worm in the team
	while WormContainerName ~= "" do
		local lock, worm = EditContainer(WormContainerName)
		
		if worm.TeamIndex == LocalTeamIndex then
			if SetBigHealth == true then
				worm.Energy = BigHealth
				SetBigHealth = false
			else
				worm.Energy = SmallHealth
			end
		end
				
		CloseContainer(lock)
		
		WormIndex = WormIndex + 1
		WormContainerName = lib_GetWormContainerName(WormIndex)
	end
	
	SetBigHealth = true
end

------------------------------------------------------------------------
function ApplyWormpotDamageScale(ContainerName, Scale)

	local ContainerLock, Container = EditContainer(ContainerName)
	
	Container.WormDamageMagnitude = Container.WormDamageMagnitude * Scale
	Container.LandDamageRadius = Container.LandDamageRadius * Scale
	
	CloseContainer(ContainerLock)
end
	
------------------------------------------------------------------------
function ApplyWormpotParticleDamageScale(ContainerName, Scale)

	local ContainerLock, Container = EditContainer(ContainerName)

	Container.ParticleCollisionWormDamageMagnitude = Container.ParticleCollisionWormDamageMagnitude * Scale
	
	CloseContainer(ContainerLock)
end

------------------------------------------------------------------------
function ApplyWormpotPowerScale(ContainerName, Scale)

	local ContainerLock, Container = EditContainer(ContainerName)
	
	Container.ImpulseMagnitude = Container.ImpulseMagnitude * Scale
	
	CloseContainer(ContainerLock)
end
	
------------------------------------------------------------------------
function ApplyWormpotParticlePowerScale(ContainerName, Scale)

	local ContainerLock, Container = EditContainer(ContainerName)
	
	Container.ParticleCollisionWormImpulseMagnitude = Container.ParticleCollisionWormImpulseMagnitude * Scale
	Container.ParticleCollisionWormImpulseYMagnitude = Container.ParticleCollisionWormImpulseYMagnitude * Scale
	
	CloseContainer(ContainerLock)
end

------------------------------------------------------------------------
function SetWeaponWind(ContainerName, IsAffectedByWind)

	local ContainerLock, Container = EditContainer(ContainerName)
	Container.IsAffectedByWind = IsAffectedByWind
	CloseContainer(ContainerLock)
end

------------------------------------------------------------------------
function SetParticleWind(ContainerName, IsAffectedByWind)

	local ContainerLock, Container = EditContainer(ContainerName)
	Container.ParticleIsEffectedByWind = IsAffectedByWind
	CloseContainer(ContainerLock)
end


function SetStickySlippyMode(Scale)
   local f = GetData("Worm.SlideStopVel")
	f = f * Scale
	SetData("Worm.SlideStopVel",f)
		
	f = GetData("Worm.BounceMultiplier")
	f = f * ( 1.0 / Scale)
	SetData("Worm.BounceMultiplier",f)


   local muliplier = (1.0 / Scale)
   ApplyWormpotStickyScale("kWeaponBananaBomb", muliplier)
   ApplyWormpotStickyScale("kWeaponClusterGrenade", muliplier)
   ApplyWormpotStickyScale("kWeaponDynamite", muliplier)
   ApplyWormpotStickyScale("kWeaponGasCanister", muliplier)
   ApplyWormpotStickyScale("kWeaponGrenade", muliplier)
   ApplyWormpotStickyScale("kWeaponHolyHandGrenade", muliplier)
end


function ApplyWormpotStickyScale(ContainerName, Scale)
   local ContainerLock, Container = EditContainer(ContainerName)
	local NewParallelMinBounceDamping = Container.ParallelMinBounceDamping * Scale
   local NewTangentialMinBounceDamping = Container.TangentialMinBounceDamping * Scale
   if NewParallelMinBounceDamping > 1 then
      NewParallelMinBounceDamping = 1
   end   
   if NewTangentialMinBounceDamping > 1 then
      NewTangentialMinBounceDamping = 1
   end
   Container.ParallelMinBounceDamping = NewParallelMinBounceDamping
   Container.TangentialMinBounceDamping = NewTangentialMinBounceDamping
	CloseContainer(ContainerLock)
end 

------------------------------------------------------------------------
function DisallowAllWeapons(WormContainer)

    WormContainer.AllowBazooka = 0;             
    WormContainer.AllowGrenade = 0;            
    WormContainer.AllowClusterGrenade = 0;      
    WormContainer.AllowAirstrike = 0;           
    WormContainer.AllowDynamite = 0;            
    WormContainer.AllowHolyHandGrenade = 0;     
    WormContainer.AllowBananaBomb = 0;          
    WormContainer.AllowLandmine = 0;            
    WormContainer.AllowShotgun = 0;             
    WormContainer.AllowBaseballBat = 0;         
    WormContainer.AllowProd = 0;                
    WormContainer.AllowFirePunch = 0;           
    WormContainer.AllowHomingMissile = 0;       
    WormContainer.AllowSheep = 0;               
    WormContainer.AllowGasCanister = 0;         
    WormContainer.AllowOldWoman = 0;            
    WormContainer.AllowConcreteDonkey = 0;      
    WormContainer.AllowSuperSheep = 0;          
    WormContainer.AllowGirder = 0;             
    -- WormContainer.AllowBridgeKit = 0;          
    WormContainer.AllowNinjaRope = 0;          
    WormContainer.AllowParachute = 0;          
    WormContainer.AllowLowGravity = 0;         
    -- WormContainer.AllowTeleport = 0;           
    WormContainer.AllowJetpack = 0;            
    WormContainer.AllowSkipGo = 1;             
    WormContainer.AllowSurrender = 1;          
    WormContainer.AllowChangeWorm = 0;         
    WormContainer.AllowRedbull = 0;          
    WormContainer.AllowArmour = 0;   
    WormContainer.AllowStarburst = 0;
    WormContainer.AllowWeaponFactoryWeapon = 0;
    WormContainer.AllowAlienAbduction = 0;
    WormContainer.AllowFatkins = 0;
    WormContainer.AllowScouser = 0;
    WormContainer.AllowNoMoreNails = 0;
    -- WormContainer.AllowPipe = 0;
    WormContainer.AllowPoisonArrow = 0;
    WormContainer.AllowSentryGun = 0;
    WormContainer.AllowSniperRifle = 0;
    WormContainer.AllowSuperAirstrike = 0;
    WormContainer.AllowBubbleTrouble = 0;
    WormContainer.AllowFlood = 0;  
    
end

