function Initialise()
	
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "Human_Team")
	lib_SetupWorm(0, "Director")
        lib_SetupWorm(1, "MakeUp")
        lib_SetupWorm(2, "Actor1")
	lib_SetupWorm(3, "Actor2")
        
	lib_SetupWorm(4, "Wizard1")
	lib_SetupWorm(5, "Wizard2")
	lib_SetupWorm(6, "Wizard3")


	SendMessage("WormManager.Reinitialise")
    
        PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Outtake")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
        SendMessage("GameLogic.Mission.Success")
end
