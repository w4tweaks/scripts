function Initialise()

    SetData("RoundTime", 3000000)
    
    --worms and stuff cloned from databank
    lib_SetupTeam(0, "Player_Team")
    lib_SetupTeam(1, "CPU_Team")

	lib_SetupWorm(0, "Player_Worm")
    lib_SetupWorm(1, "CPU_Worm1")
    lib_SetupWorm(2, "CPU_Worm2")
	lib_SetupWorm(3, "CPU_Worm3")
    lib_SetupWorm(4, "CPU_Worm4")
    lib_SetupWorm(5, "CPU_Worm5")
    SendMessage("WormManager.Reinitialise")

    WormAILevel = "AIParams.CPU1"
    CopyContainer(WormAILevel, "AIParams.Worm01")
    CopyContainer(WormAILevel, "AIParams.Worm02") 
    CopyContainer(WormAILevel, "AIParams.Worm03")
    CopyContainer(WormAILevel, "AIParams.Worm04")
    CopyContainer(WormAILevel, "AIParams.Worm05")
    
	lib_SetupTeamInventory(1, "CPU_Inventory")
    lib_SetupTeamInventory(0, "Player_Inventory")
		
	BaddieDead = 0
    water = 0    
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 3000         
    CloseContainer(lock)
    
	PlayIntroMovie()
    
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    
     local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then
    
        --raise the water on the first turn
        water = water + 20
        SetData("Water.Level", water)
    
        StartFirstTurn()
    end
end

function DoOncePerTurnFunctions()
    --raise the water every turn
    
   
    
    SetData("CommentaryPanel.Delay", 3000) -- 3 seconds
    SetData("CommentaryPanel.Comment", "M.Wild.Doom.NEW.2")
    SendMessage("CommentaryPanel.TimedText")
    
    water = water + 10
    SetData("Water.Level", water)

end

function TurnEnded()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
        SetData("GameToFrontEndDelayTime",1000)
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    else

        StartTurn()
        
    end
	
end

function Worm_Died()

	DeadWorm = GetData("DeadWorm.Id")
	
	if DeadWorm == 0 then
	
		SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
	
	else
	
		BaddieDead = BaddieDead + 1
	end
    
    if BaddieDead == 5 then
    
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
	
    elseif BaddieDead == 5 and DeadWorm == 0 then
        
		SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
	end
end
	
	
	