
-- Tutorial 3
--
-- For me to do:

-- Waiting on code requests:

-- Waiting on art requests:

-- Helpful:

function Initialise()
    kDialogueBoxTimeDelay = 0500    -- This is the delay between selecting a weapon and having the game display a dialogue box.
    kVariables()                    -- Lets have a butchers at the variables we're gonna use before the deathmatch begins.

    SendMessage("Commentary.NoDefault")

	SetData("Mine.DudProbability", 0)
	SetData("Mine.MinFuse", 5000)
	SetData("Mine.MaxFuse", 5000)
    SendMessage("GameLogic.PlaceObjects")
	
	SetData("HotSeatTime", 0)
	SetData("RoundTime", -1)
	SetData("TurnTime",60000)
	
    lib_SetupTeam(0, "Team_Human")
    lib_SetupTeam(1, "Team_Enemy")
    
    lib_SetupWorm(15, "Actor.Worm15.WorminkleDinghy")
    lib_SetupWorm(14, "Actor.Worm14.WorminkleAnchor")
    lib_SetupWorm(13, "Actor.Worm13.MikeAtopPlatform")
    
    lib_SetupWorm(7, "Actor.Worm07.Enemy4")
    lib_SetupWorm(6, "Actor.Worm06.Enemy3")
    lib_SetupWorm(5, "Actor.Worm05.Enemy2")
    lib_SetupWorm(4, "Actor.Worm04.Enemy1")
    
    SendMessage("WormManager.Reinitialise")

    SetData("Trigger.Visibility",0)
    SetData("Jetpack.InitFuel", 50000)

    SetData("Land.Indestructable", 0)

    SetData("EFMV.GameOverMovie", "EFMV.Outro")

    lock, scheme = EditContainer("GM.SchemeData")
    scheme.HelpPanelDelay = 0          
    CloseContainer(lock)
	
	Create_Crate_Sentry()
	
    SetData("EFMV.Unskipable", 0)
    -- Kick the game off by running the first little EFMV snippet.    
    kPlayEFMV("EFMV.Intro")
end

function EFMV_Terminated()
	local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "EFMV.Intro" then        
        kUnspawnAWormQuietly(15)
        kUnspawnAWormQuietly(13)
        
        lib_SetupWorm(11, "Actor.Worm11.MikeDialogue")
        lib_SetupWorm(10, "Actor.Worm10.WorminkleControls")
        lib_SetupWorm(9, "Actor.Worm09.MikeControls")
        lib_SetupWorm(8, "Actor.Worm08.DummyHuman")
        lib_SetupWorm(0, "Worm.Game1.Human0")
        
        SendIntMessage("Worm.Respawn", 11)
        SendIntMessage("Worm.Respawn", 10)
        SendIntMessage("Worm.Respawn", 9)
        SendIntMessage("Worm.Respawn", 8)
        SendIntMessage("Worm.Respawn", 0)
        
        SetData("EFMV.Unskipable", 1)
        kPlayEFMV("EFMV.Intro_Dialogue")
    elseif WhichMovie == "EFMV.Intro_Dialogue" then
        SetData("EFMV.Unskipable", 0)
        kUnspawnAWormQuietly(14)
        kUnspawnAWormQuietly(11)
        kUnspawnAWormQuietly(8)
        kPlayEFMV("EFMV.Intro2")
    elseif WhichMovie == "EFMV.Intro2" then
        SetData("EFMV.Unskipable", 1)
        kPlayEFMV("EFMV.Intro2_Dialogue")
    elseif WhichMovie == "EFMV.Intro2_Dialogue" then
        SetData("EFMV.Unskipable", 0)
        kPlayEFMV("EFMV.Intro3")
    elseif WhichMovie == "EFMV.Intro3" then
        SetUpWormsAfter_EFMV_Intro()
    elseif WhichMovie == "EFMV.FirstHealthCrate" 
    or WhichMovie == "EFMV.FirstHealthCrate2" then
        SetData("EFMV.Unskipable", 0)
        kDontStartNextTurn = false
        StartTurn()
    end
end

function SetUpWormsAfter_EFMV_Intro()    
    kUnspawnAWormQuietly(14)
    kUnspawnAWormQuietly(12)
    kUnspawnAWormQuietly(11)
    kUnspawnAWormQuietly(10)
    kUnspawnAWormQuietly(9)
    kUnspawnAWormQuietly(8)
    kUnspawnAWormQuietly(0)

    -- Setup all the worms needed for the game.
    lib_SetupWorm(0, "Worm.Game1.Human0")
    lib_SetupWorm(1, "Worm.Game1.Human1")
    lib_SetupWorm(2, "Worm.Game1.Human2")
    lib_SetupWorm(3, "Worm.Game1.Human3")
    
    -- Spawn all the buggers.
    SendIntMessage("Worm.Respawn", 0)
    SendIntMessage("Worm.Respawn", 1)
    SendIntMessage("Worm.Respawn", 2)
    SendIntMessage("Worm.Respawn", 3)

    -- Inventories for each team.
    lib_SetupTeamInventory(0, "Inventory_Human_Game2")
    lib_SetupTeamInventory(1, "Inventory_Enemy")
    
    -- Make enemy drop "Space" gravestones! OoooooOOOOHHH!!!
    lock, team_data = EditContainer("Team.Data01")
    team_data.GraveIndex = 6 -- 255 would drop no graves
    CloseContainer(lock)
    
    Create_Triggers_HealthCrate()
    
    StartFirstTurn()
end

function Game_BriefingDialogNowOff()
    if g_BoxId == "T3.Game2.Success" then
        kSendSuccessMessage()
    elseif g_BoxId == "T3_R.Failure" then
        kSendFailureMessage()
    elseif g_BoxId == "T3_R.Success" then
        kSendSuccessMessage()
--~     elseif g_BoxId == "T3_R.LandscapeIndest" then
--~         WhichWeaponWasSelected()
    end
end

function TurnStarted()
    kTurnCounter = kTurnCounter + 1
    
    if kSpawningHealthCratesHasBegun == true then
        if kTurnsToNextHealthCrate > 0 then
            kTurnsToNextHealthCrate = kTurnsToNextHealthCrate - 1
        end
    end
    
    if kTurnCounter == 1 then
        kUnspawnAWormQuietly(4)
        kUnspawnAWormQuietly(5)
        kUnspawnAWormQuietly(6)
        kUnspawnAWormQuietly(7)
        lib_SetupWorm(4, "Worm.Game2.Enemy0")
        lib_SetupWorm(5, "Worm.Game2.Enemy1")
        lib_SetupWorm(6, "Worm.Game2.Enemy2")
        lib_SetupWorm(7, "Worm.Game2.Enemy3")
        -- Set the CPU AI for each Cyber Worm.
        CopyContainer("AIParams.CPU1", "AIParams.Worm04") 
        CopyContainer("AIParams.CPU1", "AIParams.Worm05") 
        CopyContainer("AIParams.CPU1", "AIParams.Worm06") 
        CopyContainer("AIParams.CPU1", "AIParams.Worm07") 
        SendIntMessage("Worm.Respawn", 4)
        SendIntMessage("Worm.Respawn", 5)
        SendIntMessage("Worm.Respawn", 6)
        SendIntMessage("Worm.Respawn", 7)
    
        StartTimer("Delay_FirstDialogueBox", 1000)
        SetData("HotSeatTime", 3000)
    elseif kTurnCounter == 3 then
        StartTimer("Delay_HUDAnim", 0500)
        StartTimer("Delay_WindDialogueBox", 1200)
        SetData("HotSeatTime", 3000)        
    end
end

function Delay_FirstDialogueBox()
    kMakeBriefingBox("T3_R.Opening")
end

function Delay_HUDAnim()
    kPlayEFMV("HUD.Wind")
end

function Delay_WindDialogueBox()
    kMakeBriefingBox("T3_R.Wind")
end

function TurnEnded()
    if kTurnCounter == 2 then
        SetData("HotSeatTime", 0)

        kDontStartNextTurn = true
        SetData("EFMV.Unskipable", 1)
        if kPlayHealthCrateEFMV == 1 then
            lib_SpawnCrate("HealthCrate.1")
            kPlayEFMV("EFMV.FirstHealthCrate")
        elseif kPlayHealthCrateEFMV == 2 then
            lib_SpawnCrate("HealthCrate.2")
            kPlayEFMV("EFMV.FirstHealthCrate2")
        end
    end

    if kNumberOfEnemyAlive == 0 then
        kMakeBriefingBox("T3_R.Success")
    else
        if kNumberOfHumanAlive == 0 then
            kMakeBriefingBox("T3_R.Failure")
        else
            if kDontStartNextTurn == false then
                StartTurn()
            end
        end
    end
end

function DoOncePerTurnFunctions()
    local team = GetData("CurrentTeamIndex")

    --if team == 0 then return end

    if kTurnsToNextHealthCrate < 1 then
        kTurnsToNextHealthCrate = 4
    
        if kSpawningHealthCrates == true then
            kRandomHealthText()
        
            if kSpawnWhichCrateNext == 1 then
                kSpawningHealthCrates = false
                lib_SpawnCrate("Health.Spawn1")
            elseif kSpawnWhichCrateNext == 2 then
                kSpawningHealthCrates = false
                lib_SpawnCrate("Health.Spawn2")
            elseif kSpawnWhichCrateNext == 3 then
                kSpawningHealthCrates = false
                lib_SpawnCrate("Health.Spawn3")
            end
        end
    end
end

function Crate_Collected()
	CrateNumber = GetData("Crate.Index")
    
    if CrateNumber == 2 then
        kSpawningHealthCrates = true
        kSpawningHealthCratesHasBegun = true
    elseif CrateNumber == 3 then
        kSpawningHealthCrates = true
        kSpawnWhichCrateNext = kSpawnWhichCrateNext + 1
        kTurnsToNextHealthCrate = 4
        if kSpawnWhichCrateNext > 3 then
            kSpawnWhichCrateNext = 1
        end
	elseif CrateNumber == 169 then
		lib_Comment("Text.kWeaponSentryGun")
    end
end

function Crate_Destroyed()
    CrateNumber = GetData("Crate.Index")
    
    if CrateNumber == 3 then
        kSpawningHealthCrates = true
        kSpawnWhichCrateNext = kSpawnWhichCrateNext + 1
        kTurnsToNextHealthCrate = 4
        if kSpawnWhichCrateNext > 3 then
            kSpawnWhichCrateNext = 1
        end
    end
end

function Crate_Sunk()
    CrateNumber = GetData("Crate.Index")
    
    if CrateNumber == 3 then
        kSpawningHealthCrates = true
        kSpawnWhichCrateNext = kSpawnWhichCrateNext + 1
        kTurnsToNextHealthCrate = 4
        if kSpawnWhichCrateNext > 3 then
            kSpawnWhichCrateNext = 1
        end
    end
end

function Worm_Died()
    if kWeAreUnspawningWorms == false then
        deadworm = GetData("DeadWorm.Id")
        
        if deadworm == 0
        or deadworm == 1
        or deadworm == 2
        or deadworm == 3 then
            kNumberOfHumanAlive = kNumberOfHumanAlive - 1
        elseif deadworm == 4
        or deadworm == 5
        or deadworm == 6
        or deadworm == 7 then
            kNumberOfEnemyAlive = kNumberOfEnemyAlive - 1
            
            if kNumberOfEnemyAlive == 1 then
                StartTimer("Delay_SecondToLastWormDeadDialogueBox", 0700)
            end
        end
    end
end

function Delay_SecondToLastWormDeadDialogueBox()
    kMakeBriefingBox("T3_R.OneWormLeft")
end

function Weapon_Selected()
    local worm = lib_QueryWormContainer()
    
    kWormTeamIndex = worm.TeamIndex
    kWormWeaponIndex = worm.WeaponIndex

    if worm.TeamIndex == 1 then return end

	if worm.WeaponIndex ~= "UtilityJetpack" then
	    if kThisIsTheFirstWeaponSelected == true then
			StartTimer("FirstWeapon_DBox", kDialogueBoxTimeDelay)
		end
	end
end

function FirstWeapon_DBox()
    kThisIsTheFirstWeaponSelected = false
    kMakeBriefingBox("T3_R.LandscapeIndest")
end

function Sheep_DBox()
    kSheepSelected = true
    kMakeBriefingBox("T3_R.Sheep")
end

function Sentry_DBox()
    kSentryGunSelected = true
    kMakeBriefingBox("T3_R.Sentry")
end

function Jetpack_DBox()
    kJetpackSelected = true
    kMakeBriefingBox("T3_R.Jetpack")
end

--~ function WhichWeaponWasSelected()
--~     if kWormWeaponIndex == "WeaponSheep" then
--~         kSheepSelected = true
--~         kMakeBriefingBox("T3_R.Sheep")
--~     elseif kWormWeaponIndex == "WeaponSentryGun" then
--~         kSentryGunSelected = true
--~         kMakeBriefingBox("T3_R.Sentry")
--~     elseif kWormWeaponIndex == "UtilityJetpack" then
--~         kJetpackSelected = true
--~         kMakeBriefingBox("T3_R.Jetpack")
--~     end
--~ end

function Trigger_Destroyed()
	TriggerIndex = GetData("Trigger.Index")
    
	if TriggerIndex == 1 then
        kPlayHealthCrateEFMV = 2
    end
end

-- ***************************************************
-- **                VARIABLE ROOM                  **
-- ***************************************************

function kVariables()
    kThisIsFirstTurn = true
    kNumberOfHumanAlive = 4                     -- Counts how many human worms are alive
    kNumberOfEnemyAlive = 4                     -- Counts how many enemy worms are alive
    kWarnAboutDamage = 0                        -- Set to 1 when is about to warn of damage, set to 2 after warning and used to spawn health crates
    kWeAreUnspawningWorms = false               -- Set to true when unspawning worms because otherwise "function Worm_Died" picks up on it.
    kTurnCounter = 0                            -- Counts what turn we're on.
    
    kSheepSelected = false                      -- Have any of these weapons been selected yet?
    kJetpackSelected = false
    kSentryGunSelected = false
    
    kDontStartNextTurn = false                  -- If this is set to true, the turn won't be started.
    
    kPlayHealthCrateEFMV = 1                    -- Play which health crate efmv, 1 or 2? (Depends on whether or not landscape has been destroyed.)
    
    kThisIsTheFirstWeaponSelected = true        -- Set to false after a weapon has been selected.
    
    kSpawningHealthCratesHasBegun = false       -- set to true when we start spawning crates
    kSpawningHealthCrates = false               -- Set to true and we're spawning health crates.
    kSpawnWhichCrateNext = 1                    -- Which crate is gonna get dropped next, hmmm!?!
    kTurnsToNextHealthCrate = 4                 -- How many turns until the next crate is spawned.
end

-- ***************************************************
-- **                 ENGINE ROOM                   **
-- ***************************************************

function kDeleteTrigger(kWhichTrigger)
    SendIntMessage("GameLogic.DestroyTrigger",kWhichTrigger)
end

function kMakeBriefingBox(kSayWhat)
    g_BoxId = kSayWhat
    --lib_CreateBriefingBox(g_BoxId)
    lib_CreateWXBriefingBox("WXFEP.InGameBriefMiddle",g_BoxId,"FE.FramedGfx:n")
end

function kPlayEFMV(kWhatIsEFMVCalled)
    SetData("EFMV.MovieName", kWhatIsEFMVCalled)
    SendMessage("EFMV.Play") 
end

function kTurnOnWormArtillery(StrWormData)
    lock, worm = EditContainer(StrWormData)
    worm.ArtilleryMode = true
    CloseContainer(lock)
end

function kTurnOffWormArtillery(StrWormData)
    lock, worm = EditContainer(StrWormData)
    worm.ArtilleryMode = false
    CloseContainer(lock)
end

function kSendSuccessMessage()
    SendMessage("GameLogic.Mission.Success")
end

function kSendFailureMessage()
    SendMessage("GameLogic.Tutorial.Failure")
end

function kUnspawnAWormQuietly(kUnspawnWhichWorm)
    kWeAreUnspawningWorms = true
    SendIntMessage("WXWormManager.UnspawnWorm", kUnspawnWhichWorm)
    kWeAreUnspawningWorms = false
end

function kRandomHealthText()
    TextToDisplay = {"Comment.Health.5","Comment.Health.6","Comment.Health.7","Comment.Health.8","Comment.Health.9"}
    local myRandomInteger = lib_GetRandom(1, 5)
        
    lib_Comment(TextToDisplay[myRandomInteger])
end

-- ***************************************************
-- **                    CRATES                     **
-- ***************************************************

function Create_Crate_HealthCrate()
    lib_SpawnCrate("HealthCrate.Game1.25")
end

function Create_Crate_Sentry()
    lib_SpawnCrate("WeaponCrate.Sentry")
end


-- Triggers

function Create_Triggers_HealthCrate()
    lib_SpawnTrigger("Trig.Health1")
end
