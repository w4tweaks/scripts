

-- ROB IN THE HOOD NEW VERSION!!!!!!!!

function Initialise()

	SetData("TurnTime", 60000)
	--SetData("Trigger.Visibility", 1)
    --SetData("Land.Indestructable", 1)
    SendIntMessage("Crate.RadarHide", 4) 
    SendIntMessage("Crate.RadarHide", 5)
    SendIntMessage("Crate.RadarHide", 6)

    
	-- cage triggers
	lib_SpawnTrigger("Trig0")
	lib_SpawnTrigger("Trig1")
	lib_SpawnTrigger("Trig2")
	
    lib_SpawnCrate("Crate4")
    lib_SpawnCrate("Crate5")
    lib_SpawnCrate("Crate6")
    lib_SpawnCrate("Crate7")
    
    
	--set up the human worm and team - called from databank
	lib_SetupTeam(0, "Team Human")
    
    lib_SetupWorm(0, "Dude")
	lib_SetupWorm(1, "Duder")
	lib_SetupWorm(2, "Dudek")
	
	--set up the soldiers and wizard
	lib_SetupTeam(1, "Team CPU")
	
	lib_SetupWorm(3, "Soldier1")
	lib_SetupWorm(4, "Soldier2")
	lib_SetupWorm(5, "Soldier3")
        
        WormAILevel = "AIParams.CPU2"
        CopyContainer(WormAILevel, "AIParams.Worm03")
        CopyContainer(WormAILevel, "AIParams.Worm04")        
        CopyContainer(WormAILevel, "AIParams.Worm05")
        CopyContainer(WormAILevel, "AIParams.Worm06")         

	SendMessage("WormManager.Reinitialise")
	
	lib_SetupTeamInventory(0, "Inventory Human")
	lib_SetupTeamInventory(1, "Inventory CPU")

    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 2000         
    CloseContainer(lock)

    DeadPlayer = 0
    DeadEnemy = 0 
    MovieHasPlayed = false
    
    RedbullWormDead = false
    JetpackWormDead = false
    
    Worm2isdead = false
    Worm3isdead = false
    
    
    HumanTeamDead = false
    
	--CrateCount = 0
	--TrigCount = 0
	
	--SetWind()
	PlayIntroMovie()
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end


function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then
        
        SendIntMessage("Crate.Delete", 4)
        SendIntMessage("Crate.Delete", 5)
        SendIntMessage("Crate.Delete", 6)
        StartFirstTurn()
    
    
    elseif WhichMovie == "Midtro" then
        
        SpawnCrate4()
                
        lib_CreateExplosion("Crate4", 55, 0, 0, 40, 1)
        
        local DataID = lib_GetTeamInventoryName(1)
        lock, inventory = EditContainer(DataID) 
        inventory.BananaBomb = 5
        inventory.Fatkins = 3
        inventory.HolyHandGrenade = 3
        CloseContainer(lock)
        
	end
   
end



function Worm_Died()
    
    deadWorm = GetData("DeadWorm.Id")
    
    -- if a player worm dies then add to the deadplayer variable
    -- if an AI worm dies then add to the variable
    -- if the wizard is killed then mission sucess (play outro)
    -- or if all 3 player worms are killed then game over fail
    -- if all 3 enemy worms are killed but the midtro hasn't played
    -- then play the midtro
    
    
    if deadWorm == 5 then  -- Set to spawn crate
        
        Worm2isdead = true
        
    elseif deadWorm == 4 then -- Set to spawn crate
    
        Worm3isdead = true
        
    end
        
    if deadWorm < 3 then --  Players worms index numbers
    
        DeadPlayer = DeadPlayer + 1

    
        elseif deadWorm > 2 then

            DeadEnemy = DeadEnemy + 1
            
            
            if deadWorm == 3 and Worm2isdead == false then
        
        
                lib_SpawnCrate("Crate1")
            
            elseif deadWorm == 5 and Worm3isdead == false then
        
                lib_SpawnCrate("Crate2")
            
            elseif deadWorm == 6 then -- Wizard worm dead
            
                SetData("EFMV.GameOverMovie", "Outro")
                SendMessage("GameLogic.Mission.Success") 
                lib_DisplaySuccessComment()
            
            
            end
        
    end

    if DeadPlayer == 3 then -- All players worms dead
        
        HumanTeamDead = true
        
    end
    
 
    if DeadEnemy == 3 and MovieHasPlayed == false then
        
        lib_SetupWorm(6, "Wally the Wizard")
        SendIntMessage("Worm.Respawn", 6) 
        
        
        
        SetData("EFMV.MovieName", "Midtro")
        SendMessage("EFMV.Play")
        MovieHasPlayed = true
--    
    end
end

function SpawnCrate4()  

    lib_SpawnCrate("Crate4")
    StartTimer("SpawnCrate5", 2000)
end

function SpawnCrate5()
    lib_CreateExplosion("Crate5", 55, 0, 0, 40, 1)
    lib_SpawnCrate("Crate5")
    StartTimer("SpawnCrate6", 2000)
end

function SpawnCrate6()
     lib_CreateExplosion("Crate6", 55, 0, 0, 40, 1)
    lib_SpawnCrate("Crate6")
end
    


function TurnEnded()
    
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
        SetData("GameToFrontEndDelayTime",1000)
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    elseif HumanTeamDead == true then
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    else
    
        StartTurn()
    end
end

