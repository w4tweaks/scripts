--Penguin

function Initialise()
	SetData("Mine.DudProbability", 0)
	SetData("Mine.MinFuse", 5000)
	SetData("Mine.MaxFuse", 5000)
    SendMessage("GameLogic.PlaceObjects")	
	
	SetData("HotSeatTime", 3000) 
	SetData("RoundTime", 0)
	SetData("TurnTime", 45000) 

	SetData("Land.Indestructable", 1)
	
    lib_SetupTeam(0, "Player_Team")
    lib_SetupTeam(1, "CPU_Team")
    lib_SetupTeam(2, "Worminkle_Team")
    
    lib_SetupWorm(0, "Player_Worm0")
    lib_SetupWorm(1, "Player_Worm1")
    lib_SetupWorm(2, "Player_Worm2")
    lib_SetupWorm(3, "Player_Worm3")
    
    lib_SetupWorm(4, "CPU_Worm0")
    lib_SetupWorm(5, "CPU_Worm1")
    lib_SetupWorm(6, "CPU_Worm2")
    
    lib_SetupWorm(7, "Worminkle_Worm0")
    
    SendMessage("WormManager.Reinitialise")

    lib_SetupTeamInventory(0, "Player_Inventory")
    lib_SetupTeamInventory(1, "CPU_Inventory")
    lib_SetupTeamInventory(2, "Worminkle_Inventory")

    --SetData("Trigger.Visibility", 1)
    --lib_SpawnTrigger("Trigger1")
	
	StartFirstTurn()
end

--function Trigger_Collected()

--	TriggerIndex = GetData("Trigger.Index")
--       
--	if TriggerIndex == 1 then
--    
--        Bang1()
--    
--	elseif TriggerIndex == 2 then
--    
--        Bang2()
--    
--	elseif TriggerIndex == 3 then
--    
--        Bang3()
--    
--	elseif TriggerIndex == 4 then
--    
--        Bang4()
--    
--	end  
--    
--end

--function Trigger_Destroyed()

--	TriggerIndex = GetData("Trigger.Index")

--	
--	if TriggerIndex == 5 then
--    
--        nTotalHits = nTotalHits + 1
--        lib_SpawnTrigger("MainTrigger")
--    
--	elseif TriggerIndex == 6 then
--    
--		SendMessage("GameLogic.Mission.Failure")
--        
--	elseif TriggerIndex == 7 then
--    
--		SendMessage("GameLogic.Mission.Failure")	
--        
--	elseif TriggerIndex == 8 then
--    
--		SendMessage("GameLogic.Mission.Failure")	
--        
--	elseif TriggerIndex == 9 then
--    
--		SendMessage("GameLogic.Mission.Failure")	
--        
--	end
--	
--	
--end
	

--function Bang1()
--	lib_CreateExplosion("bang1", 150, 0.55, 100, 100, 100)
--end

--function Bang2()
--	lib_CreateExplosion("bang2", 150, 0.55, 100, 100, 100)
--end

--function Bang3()
--	lib_CreateExplosion("bang3", 150, 0.55, 100, 100, 100)
--end

--function Bang4()
--	lib_CreateExplosion("bang4", 150, 0.55, 100, 100, 100)
--end

-----------------------------------------------------------------------------------------------
--function  TurnEnded()

--	TeamCount = lib_GetActiveAlliances()
--	WhichTeam = lib_GetSurvivingTeamIndex()
--	
--	if nTotalHits == 3 then
--    
--        lib_CreateEmitter("Part_ElectricSpark","Spark1")
--        lib_CreateEmitter("Part_ElectricSpark","Spark2")
--    

--    
--	elseif nTotalHits == 6 then
--    
--        lib_CreateEmitter("Part_ElectricSpark","Spark3")
--        lib_CreateEmitter("Part_ElectricSpark","Spark4")
--    
--	elseif nTotalHits == 8 then
--    
--        lib_CreateEmitter("Part_ElectricSpark","Spark5")
--        lib_CreateEmitter("Part_ElectricSpark","Spark6")
--    
--	end
--	
--	
--	if nTotalHits == 10 then
--    
--		SendMessage("GameLogic.Mission.Success")	
--        
--	else
--    
--		StartTurn()
--        
--	end
--end
