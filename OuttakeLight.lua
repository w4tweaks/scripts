function Initialise()
	
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "PlayerTeam")
	lib_SetupWorm(0, "WORM0")
        lib_SetupWorm(1, "WORM1")
        lib_SetupWorm(2, "WORM2")
        lib_SetupWorm(3, "WORM3")
        lib_SetupWorm(4, "WORM4")
        lib_SetupWorm(5, "WORM5")
        lib_SetupWorm(6, "WORM6")
        lib_SetupWorm(7, "WORM7")


	SendMessage("WormManager.Reinitialise")
	
    
        PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Outtake2")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
        SendMessage("GameLogic.Mission.Success")
end
