function Initialise()
	
	SetData("Land.Indestructable", 0)
	SetData("TurnTime", 0)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "Human_Team")
	lib_SetupWorm(0, "kWORM0")
        lib_SetupWorm(1, "kWORM1")
        lib_SetupWorm(2, "kWORM2")
        lib_SetupWorm(3, "kWORM3")
        lib_SetupWorm(4, "kWORM4")
        lib_SetupWorm(5, "kWORM5")
        lib_SetupWorm(6, "kWORM6")
        lib_SetupWorm(7, "kWORM7")

        



	SendMessage("WormManager.Reinitialise")
    
        PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Outtake2")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
        SendMessage("GameLogic.Mission.Success")
end
