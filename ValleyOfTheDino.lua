function Initialise()

	SetData("TurnTime", 30000)
    --SetData("RoundTime", 45000)
	SendMessage("GameLogic.PlaceObjects")
    
	--setup teams and worms from d/bank
	lib_SetupTeam(0, "HumanTeam")
	lib_SetupTeam(1, "EnemyTeam")
    lib_SetupTeam(2, "OutroTeam")
	
	lib_SetupWorm(0, "Worm1")
	lib_SetupWorm(1, "Worm2")
	lib_SetupWorm(2, "Worm3")
	lib_SetupWorm(3, "Worm4")
    
    lib_SetupWorm(4, "Prof")
	
	lib_SetupWorm(5, "Enemy1")
	lib_SetupWorm(6, "Enemy2")
	lib_SetupWorm(7, "Enemy3")
	lib_SetupWorm(8, "Enemy4")
	lib_SetupWorm(9, "Enemy5")
	lib_SetupWorm(10, "Enemy6")

	lib_SetupWorm(11, "Enemy7")
	lib_SetupWorm(12, "Enemy8")
	lib_SetupWorm(13, "Enemy9")

	SendMessage("WormManager.Reinitialise")

        WormAILevel = "AIParams.CPU3"        
        CopyContainer(WormAILevel, "AIParams.Worm05")   
        CopyContainer(WormAILevel, "AIParams.Worm06")
        CopyContainer(WormAILevel, "AIParams.Worm07")     
        CopyContainer(WormAILevel, "AIParams.Worm08") 
        CopyContainer(WormAILevel, "AIParams.Worm09")
        CopyContainer(WormAILevel, "AIParams.Worm10")
        CopyContainer(WormAILevel, "AIParams.Worm11")
        CopyContainer(WormAILevel, "AIParams.Worm12")        
        CopyContainer(WormAILevel, "AIParams.Worm13")
        
	lib_SetupTeamInventory(0, "Inv_Human")
	lib_SetupTeamInventory(1, "Inv_Enemy")
    
--    SetData("Trigger.Visibility", 1)
    lib_SpawnTrigger("ProfTrig")

    BaddiedDead = 0
    HumanDead = 0
    ProfDead = false
      
    PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "ValleyIntro")
    SendMessage("EFMV.Play")
   
end

function EFMV_Terminated()
    
    local WhichMovie = GetData("EFMV.MovieName")
    
    
    if WhichMovie == "ValleyIntro" then
    
        StartFirstTurn()
    end
    
    
end

function PlayMidtro()
    SetData("EFMV.MovieName", "Midtro")
    SendMessage("EFMV.Play")
end
    
function PlayOutro()
    SetData("EFMV.MovieName", "Outro")
    SendMessage("EFMV.Play")
end
    

function TurnEnded()
    
    -- win and lose conditions 
--    TeamCount = lib_GetActiveAlliances()
--	WhichTeam = lib_GetSurvivingTeamIndex()
--    
--    if TeamCount == 0 or (WhichTeam == 1 and TeamCount == 1) then
--    
--        lib_DisplayFailureComment()
--        SendMessage("GameLogic.Mission.Failure")
    
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
    
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
    
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    elseif HumanDead == 4 then
    
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    elseif ProfDead == true then
    
         SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    
    else
        
        StartTurn()
    end
end

function DoOncePerTurnFunctions()
    
    if BaddiedDead == 9 then
        
        PlayMidtro()
        SpawnGirderCrates()
        
        BaddiedDead = 0
    end
end
    
function Worm_Died()

   -- who died and what is his team number?
    local deadworm = GetData("DeadWorm.Id")
    
    if deadworm < 4 then
    
        HumanDead = HumanDead + 1
    
    elseif deadworm > 4 then
        
        BaddiedDead = BaddiedDead + 1
        
        
    elseif deadworm == 4 then
    
        ProfDead = true
   
       
    
    end
end


function SpawnGirderCrates()
    
    -- spawn the girder crates from this array and loop
    local Crate = { "Crate1", "Crate2", "Crate3", "Crate4", "Crate5", "Crate6"}
                            
    for i = 1, 6 do
    
        lib_SpawnCrate(Crate[i])
    end
end


function Trigger_Collected()
    
    SendIntMessage("Worm.DieQuietly", 0) -- kill off remaining player worms
    SendIntMessage("Worm.DieQuietly", 1) -- kill off remaining player worms
    SendIntMessage("Worm.DieQuietly", 2) -- kill off remaining player worms
    SendIntMessage("Worm.DieQuietly", 3) -- kill off remaining player worms
    SendIntMessage("Worm.DieQuietly", 4) -- kill off old prof
    
    lib_SetupWorm(14, "ProfOutro")  -- new prof without tail nail
    SendIntMessage("Worm.Respawn", 14)
    
    lib_SetupWorm(6, "Outro1")
    SendIntMessage("Worm.Respawn", 6)
    
    lib_SetupWorm(7, "Outro2")
    SendIntMessage("Worm.Respawn", 7)
    
    lib_SetupWorm(8, "Outro3")
    SendIntMessage("Worm.Respawn", 8)
    
    lib_SetupWorm(10, "Outro4")
    SendIntMessage("Worm.Respawn", 10)
    
    SetData("EFMV.GameOverMovie", "Outro")
    
    SendMessage("GameLogic.Mission.Success")
    lib_DisplaySuccessComment()
end




