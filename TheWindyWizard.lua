--- The Windy Weapon

function Initialise()

    SetData("RoundTime", 3000000)
 
    SendMessage("GameLogic.PlaceObjects")	
    SetData("Trigger.Visibility", 0)
    SetData("TurnTime", 0)
    lib_SpawnTrigger("Trigger1")
    lib_SpawnTrigger("Trigger2")
    lib_SpawnTrigger("Trigger3")
    lib_SpawnTrigger("Trigger4")
    
    lib_SetupTeam(0, "PlayersTeam")
    lib_SetupTeam(1, "CPUPlayer")
    lib_SetupWorm(1, "PlayerWorm")  -- Players worm
    lib_SetupWorm(2, "Wizzard_Worm")  -- Cpu worm
    
    local lock, team = EditContainer("Team.Data01")
    

    team.ATT_Hat = "WXFE.A.Hat.Wizard"
    team.ATT_Glasses = ""
    team.ATT_Gloves = ""
    team.ATT_Tash = ""
    WizardMovieHasPlayed = false
    CloseContainer(lock)

    SendMessage("WormManager.Reinitialise")

	lib_SetupTeamInventory(1, "CPU_Inventory")
    lib_SetupTeamInventory(0, "Player_Inventory")
    
    --- AI things here 
    
    
    WormAILevel = "AIParams.CPU1"
    CopyContainer(WormAILevel, "AIParams.Worm05") -- Apprentice
    CopyContainer(WormAILevel, "AIParams.Worm06") -- Apprentice
    CopyContainer(WormAILevel, "AIParams.Worm07") -- Apprentice
    CopyContainer(WormAILevel, "AIParams.Worm09") -- The Windy Wizard 
    
    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 2000         
    CloseContainer(lock)
    
    
    EmitterCPU4 = lib_CreateEmitter("WXPL_WizCloud", "WizzardParty")
	WormIndexNumber = 0
	Destroyedtrig = 1 -- setback to 1!!! or set to 4 to skip to mid sequence 2
    NumberToBlowUp = 0
    PlayWizardMovieNumber = 0
	NumberofDeadWizzards = 0
    NumberOfWizardWormsOnLandscape = 0
    DeleteEmitteroneParticle = false
    DeleteEmittertwoParticle = false
    DeleteEmitterthreeParticle = false
    
    IslandDestroyed = {false, false, false, false}

    lib_SpawnCrate("Target1")
    lib_SpawnCrate("Target2")
    lib_SpawnCrate("Target3")
    lib_SpawnCrate("Target4")
    
    PlayIntroMovie()
	
end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "WindyWizardIntro")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Island1BlowUp"  or WhichMovie == "Island2BlowUp" or WhichMovie == "Island3BlowUp" or WhichMovie == "Island4BlowUp" then
    -- Wizard movies are piggybacked here.
    
        PlayWizardMovieNumber = PlayWizardMovieNumber + 1
    
        if PlayWizardMovieNumber == 1 then
        
            SetData("EFMV.MovieName", "WizardSpawn")
            SendMessage("EFMV.Play")
            
        elseif PlayWizardMovieNumber == 2 then
        
            SetData("EFMV.MovieName", "WizardSpawn2")
            SendMessage("EFMV.Play")
            
        elseif PlayWizardMovieNumber == 3 then
        
            SetData("EFMV.MovieName", "WizardSpawn3")
            SendMessage("EFMV.Play")
                    
        end
        
        if WizardMovieHasPlayed == true then
        
            if NumberOfWizardWormsOnLandscape == 0 then
        
--~                 SetData("SameTeamNextTurn", 1)
                
            end
            
            SendMessage("EFMV.End")

            --StartTurn()
        end

    elseif WhichMovie == "MidSequence2" then
    
        lib_DeleteEmitterImmediate(EmitterCPU4)
        WizzardDie()
        if NumberOfWizardWormsOnLandscape == 0 then
        
            --SetData("SameTeamNextTurn", 1)
                
        end
       -- SendMessage("EFMV.End")
        --StartTurn()
    
    elseif WhichMovie == "WizardSpawn" or WhichMovie == "WizardSpawn2" or WhichMovie == "WizardSpawn3" then
        --WizardMovieHasPlayed = true
--~         SetData("SameTeamNextTurn", 1)
        
        --StartTurn()
    
    elseif WhichMovie == "WindyWizardIntro" then

        lib_DeleteEmitterImmediate(EmitterCPU4)
        StartFirstTurn()
    
    end
end

function SetWind()
    local nTeam = GetData("CurrentTeamIndex")
    
    if nTeam == 0 and Destroyedtrig < 5 then
    
        local MaxWind = 0.000170
        local WindSpeed = {0.2,0.3,0.4,0.5} -- Values use for the windspeed

        SetData("Wind.Speed", WindSpeed[Destroyedtrig]*MaxWind)     --max wind, value taken from number of Triggers Destroyed.    --max wind, value taken from number of Triggers Destroyed.
        SetData("Wind.Direction", 5.5)
        
     else
     
        SetData("Wind.Speed", 0)  --no wind
        
    end
end

function Trigger_Destroyed()
    TriggerIndex = GetData("Trigger.Index")
    Destroyedtrig = Destroyedtrig + 1
    WormIndexNumber = Destroyedtrig + 3
    
    if Destroyedtrig < 5 then
    
        local WormToSpawn = {"CPUWorm_1","CPUWorm_2","CPUWorm_3"}
--~         SetData("SameTeamNextTurn", 1)
        lib_SetupWorm(WormIndexNumber,WormToSpawn[Destroyedtrig - 1]) 
        SendIntMessage("Worm.Respawn", WormIndexNumber) 
        NumberOfWizardWormsOnLandscape = NumberOfWizardWormsOnLandscape + 1

--        
        if WormIndexNumber == 5 then
        
            EmitterCPU1 = lib_CreateEmitter("WXPL_WizCloud", WormToSpawn[Destroyedtrig - 1]) -- cpu wizzard 1 cloud

          
        elseif WormIndexNumber == 6 then
        
            EmitterCPU2 = lib_CreateEmitter("WXPL_WizCloud", WormToSpawn[Destroyedtrig - 1]) -- cpu wizzard 2 cloud
            
        elseif WormIndexNumber == 7 then

            EmitterCPU3 = lib_CreateEmitter("WXPL_WizCloud", WormToSpawn[Destroyedtrig - 1]) -- cpu wizzard 3 cloud
          
        end  
    end
    
    if Destroyedtrig < 5 then
    
            local EFMVTOPLAY = {"Island1BlowUp","Island2BlowUp","Island3BlowUp","Island4BlowUp"}
    
            IslandDestroyed[TriggerIndex] = true 
            
            SendIntMessage("Crate.Delete", TriggerIndex)
    
            SetData("EFMV.MovieName", EFMVTOPLAY[TriggerIndex])
            SendMessage("EFMV.Play")

    elseif Destroyedtrig == 5 then
    
            NumberOfWizardWormsOnLandscape = NumberOfWizardWormsOnLandscape + 1
    
            SendIntMessage("Crate.Delete", TriggerIndex)
    
            if IslandDestroyed[1] == false then
            
                lib_CreateExplosion("Bang1a", 120, 3, 150, 130, 80)
            
            elseif IslandDestroyed[2] == false then
            
            
                lib_CreateExplosion("Bang2d", 120, 3, 150, 130, 80)
            
            elseif IslandDestroyed[3] == false then
            
                lib_CreateExplosion("Bang3d", 120, 3, 150, 130, 80)
                lib_CreateExplosion("Bang3e", 120, 3, 150, 130, 80)
            
            elseif IslandDestroyed[4] == false then
            
            lib_CreateExplosion("Bang4b", 120, 3, 150, 130, 80)
            lib_CreateExplosion("Bang4e", 120, 3, 150, 130, 80)
            
            end
    
            StartTimer("StartStage2EFMV",1000) 
    
    end
	
end

function TurnEnded()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
            SetData("GameToFrontEndDelayTime",1000)
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        else
        
        if NumberOfWizardWormsOnLandscape > 0 then
        
            SetData("TurnTime", 25000)
            
        elseif NumberOfWizardWormsOnLandscape == 0 then
        
            SetData("TurnTime", 0)
            
        end
        
        
        if DeleteEmitteroneParticle == true then
        
            lib_DeleteEmitterImmediate(EmitterCPU1)
            
        end
            
        if DeleteEmittertwoParticle == true then
        
            lib_DeleteEmitterImmediate(EmitterCPU2)
            
        end
            
        if DeleteEmitterthreeParticle == true then
        
            lib_DeleteEmitterImmediate(EmitterCPU3)
            
        end

            StartTurn()
            
        end
	
end


function Worm_Died()	

		
    deadWorm = GetData("DeadWorm.Id")
    
    if deadWorm == 5 then
   
       
       DeleteEmitteroneParticle = true
       NumberOfWizardWormsOnLandscape = NumberOfWizardWormsOnLandscape - 1
       
       NumberofDeadWizzards = NumberofDeadWizzards + 1
       
    elseif deadWorm == 6 then


        DeleteEmittertwoParticle = true
        
        NumberOfWizardWormsOnLandscape = NumberOfWizardWormsOnLandscape - 1
        
        NumberofDeadWizzards = NumberofDeadWizzards + 1
        
    elseif deadWorm == 7 then
    
        DeleteEmitterthreeParticle = true
    
        NumberOfWizardWormsOnLandscape = NumberOfWizardWormsOnLandscape - 1
        
        
        NumberofDeadWizzards = NumberofDeadWizzards + 1
        
    end
    
    if deadWorm == 9 then
    
        NumberofDeadWizzards = NumberofDeadWizzards + 1
        
    end
    
    if deadWorm == 1 then
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
    end
        
	if Destroyedtrig == 5 and NumberofDeadWizzards == 4 then
            SetData("EFMV.MovieName", "OutroSuccess")
            SendMessage("GameLogic.Mission.Success")
            lib_DisplaySuccessComment()
	end
	
end


function StartStage2EFMV()
        
        EmitterCPU4 = lib_CreateEmitter("WXPL_WizCloud", "WizzardParty")
        
        --lib_SetupWorm(2, "Wizzard_Worm")  -- Cpu worm
        --SendIntMessage("Worm.Respawn",2) 

        SetData("EFMV.MovieName", "MidSequence2")
        SendMessage("EFMV.Play")

        StartTimer("WizzardDie",10000)
        StartTimer("EFMV_Terminated", 14000) -- simulate end of movie
end

function WizzardDie()
    SendIntMessage("Worm.DieQuietly", 2)
    
end




