function Initialise()
	
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
    SetData("RoundTime",0)
	SetData("HotSeatTime", 0) 
    SetData("RetreatTime", 0) 
    SetData("HUD.Clock.DisplayTenths", 1)
    SetData("Mine.DudProbability", 0.0)
--~     SetData("Mine.MinFuse", 3000)
--~     SetData("Mine.MaxFuse", 3000)
--~     SendMessage("GameLogic.PlaceObjects")	
	
	lib_SetupTeam(0, "HumanTeam")
	lib_SetupWorm(0, "Player")
	lib_SetupTeamInventory(0, "Inv_Human")
	
	
	SendMessage("WormManager.Reinitialise")
    
    SetData("Mine.DetonationType", 1) 
    -- Mines will always explode as normal mines
		
    g_nNextTarget = 1
    TotalTargetNumber = 14
    
    SetData("HUD.Counter.Active", 1)
    SetData("HUD.Counter.Value", 14)
    
    SendMessage("Commentary.NoDefault")
    
    SpawnNextTarget()
   -- StartFirstTurn()
   PlayIntroMovie()
   
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    StartFirstTurn()
end

function Crate_Collected()

    TotalTargetNumber = TotalTargetNumber - 1 

    SetData("HUD.Counter.Value", TotalTargetNumber)
    
    CrateIndex = GetData("Crate.Index")
    
    TextToDisplay = {"C.Generic.Good.1","C.Generic.Good.2","C.Generic.Good.3","C.Generic.Time.1"}
        local myRandomInteger = lib_GetRandom(1, 4)
        
        lib_Comment(TextToDisplay[myRandomInteger])
    
    
    if CrateIndex == 14 then
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Success")
        
    else
        SpawnNextTarget()
    end
end


function SpawnNextTarget()

    -- target cycle 
    local Crate = {"Targ1", "Targ2", "Targ3", "Targ4", "Targ5", "Targ6", "Targ7", "Targ8", "Targ9", "Targ10", "Targ11", "Targ12", "Targ13", "Targ14"}
        lib_SpawnCrate(Crate[g_nNextTarget])
        g_nNextTarget = g_nNextTarget + 1
        
    --log("g_nNextTarget = ", g_nNextTarget)

end

function TurnEnded()
    StartTurn()
end


function Worm_Died()
    
    deadworm = GetData("DeadWorm.Id")
    
    if deadworm == 0 then
    
        lib_DisplayFailureComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Failure")
    end
end
