
--WFK

function Initialise()

    lib_SetupTeam(0, "Player_Team")
    lib_SetupTeam(1, "CPU_Team")

	lib_SetupWorm(0, "Player_Worm")  -- Players worm
    lib_SetupWorm(4, "CPU_Worm1")
    lib_SetupWorm(5, "CPU_Worm2")
    lib_SetupWorm(6, "CPU_Worm3")
    DEATHMATCH = false
    SendMessage("WormManager.Reinitialise")

    WormAILevel = "AIParams.CPU2"
    CopyContainer(WormAILevel, "AIParams.Worm01")
    CopyContainer(WormAILevel, "AIParams.Worm02") 
    CopyContainer(WormAILevel, "AIParams.Worm03")

    CopyContainer(WormAILevel, "AIParams.Worm05")  
    CopyContainer(WormAILevel, "AIParams.Worm06")
    CopyContainer(WormAILevel, "AIParams.Worm07") 
    CopyContainer(WormAILevel, "AIParams.Worm08")
    CopyContainer(WormAILevel, "AIParams.Worm09")
    CopyContainer(WormAILevel, "AIParams.Worm10") 
    
    
	lib_SetupTeamInventory(0, "Player_Inventory")
    lib_SetupTeamInventory(1, "CPU_Inventory")

    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 3000         
    CloseContainer(lock)

    BlowTheGate = false
    AlreadyPlayed = false
    WhichEnemy = false
    lib_SpawnCrate("Girder_Crate") 

	
	SetData("Land.Indestructable", 0)


----------------------------Spotted triggers--------------------------------    
		
    SetData("Trigger.Visibility", 0)
    lib_SpawnTrigger("Spot_Trigger1")
    lib_SpawnTrigger("Spot_Trigger2")
    lib_SpawnTrigger("Spot_Trigger3")
    lib_SpawnTrigger("Spot_Trigger4")
    lib_SpawnTrigger("Spot_Trigger5")
    lib_SpawnTrigger("Spot_Trigger6")
    lib_SpawnTrigger("Spot_Trigger7")
    lib_SpawnTrigger("Spot_Trigger8")
    lib_SpawnTrigger("Spot_Trigger9")
    lib_SpawnTrigger("Spot_Trigger10")
    lib_SpawnTrigger("Spot_Trigger11")
    lib_SpawnTrigger("Spot_Trigger12")
    lib_SpawnTrigger("Spot_Trigger13")
    lib_SpawnTrigger("Gate_Trigger")
    
	SetData("HotSeatTime", 5000)
	SetData("TurnTime", 60000)
  
    lock, worm = EditContainer("Worm.Data02") -- set all the cpu worms to ArtilleryMode
    worm.ArtilleryMode = true
    CloseContainer(lock)
    
    lock, worm = EditContainer("Worm.Data03")
    worm.ArtilleryMode = true
    CloseContainer(lock)
    
    lock, worm = EditContainer("Worm.Data04")
    worm.ArtilleryMode = true
    CloseContainer(lock)
    
    lock, worm = EditContainer("Worm.Data05")
    worm.ArtilleryMode = true
    CloseContainer(lock)    
    GateHasBeenBlownUp = false
    CrateOnLandscape = true
    playerwasspotted = false
    DontArmAgain = false
    TriggeredOnce = false
    PlayIntroMovie()
    
end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
    
end


function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then
        StartFirstTurn()

    elseif WhichMovie == "DestroyGate" or WhichMovie == "EnemyLook1" or WhichMovie == "EnemyLook2" then
    
        StartTurn()
                
    end
    
end


function SetWind() -- Keeps the wind nice and calm or the player.

    local myRandomInteger = lib_GetRandom(1, 4)
       MaxWind = GetData("Wind.MaxSpeed")
       SetData("Wind.Speed", 1.5*MaxWind * 2 /10)
       SetData("Wind.Direction", myRandomInteger) 
    
end

function Trigger_Collected()

    TriggerIndex = GetData("Trigger.Index")
    
    if TriggerIndex == 1 then -- Player has been spotted
    
    
        if playerwasspotted == false then
            
            playerwasspotted = true
            SendIntMessage("GameLogic.DestroyTrigger", 1)
            ArmThemCPUWorms()
            SetData("CommentaryPanel.Delay", 3000) -- 3 seconds
            SetData("CommentaryPanel.Comment", "M.Wild.HiNoon.NEW.L1")-- You Have Been Spotted!!!!!
            SendMessage("CommentaryPanel.TimedText")
            --TurnEnded()
            
        end

    end
end

function ArmThemCPUWorms()

    if DontArmAgain == false then
    
        DontArmAgain = true

        SendIntMessage("WXWormManager.UnspawnWorm", 4)
        lib_SetupWorm(4, "CPU_Worm1Respawn") 
        SendIntMessage("Worm.Respawn",4)  
    
        SendIntMessage("WXWormManager.UnspawnWorm", 5)
        lib_SetupWorm(5, "CPU_Worm3Respawn") 
        SendIntMessage("Worm.Respawn",5)  
        
        SendIntMessage("WXWormManager.UnspawnWorm", 6)
        lib_SetupWorm(6, "CPU_Worm2Respawn") 
        SendIntMessage("Worm.Respawn",6)  
          
          
        if playerwasspotted == true then
    
            lock, inventory = EditContainer("Inventory.Team01") 
        --    inventory.Bazooka = -1
        --    inventory.Grenade = -1
        --    inventory.ClusterGrenade = 5
        --    inventory.Dynamite = 2
        --    inventory.Sheep = 1
        --    inventory.Parachute = 2
            inventory.SkipGo = -1
        --    inventory.Girder = 3
            inventory.Shotgun = -1
            inventory.FirePunch = -1
            --inventory.Prod = -1
            CloseContainer(lock)
            DontArmAgain = false
        end
        
        if DEATHMATCH == true then
        
            lock, inventory = EditContainer("Inventory.Team01") 
            inventory.Bazooka = -1
            inventory.Grenade = -1
            inventory.ClusterGrenade = 5
            inventory.Dynamite = 2
            inventory.Sheep = 1
            inventory.Parachute = 2
            inventory.SkipGo = -1
            inventory.Girder = 3
            inventory.Shotgun = 4
            inventory.FirePunch = 4
            inventory.Prod = -1
            CloseContainer(lock)
            
        end
        
        
    end


        
end


function Trigger_Destroyed()

    TriggerIndex = GetData("Trigger.Index")

	if TriggerIndex == 2 then
    
        if playerwasspotted == false then
            playerwasspotted = true
    
            SetData("EFMV.MovieName", "Midtro")
            SendMessage("EFMV.Play")
    
            GateHasBeenBlownUp = true
            

            
        end
       
    lib_SpawnCrate("Bazooka_Crate") 
    lib_SpawnCrate("Grenade_Crate") 
    lib_SpawnCrate("Shotgun_Crate") 
    lib_SpawnCrate("BananaBomb_Crate") 
    lib_SpawnCrate("Sheep_Crate") 
    lib_SpawnCrate("Crate1") 
    lib_SpawnCrate("Crate2") 
    lib_SpawnCrate("Crate3") 
    lib_SetupWorm(1, "Player_WormSpawn1") 
    SendIntMessage("Worm.Respawn", 1)   
    
    lib_SetupWorm(2, "Player_WormSpawn2") 
    SendIntMessage("Worm.Respawn", 2)   
    
    lib_SetupWorm(3, "Player_WormSpawn3") 
    SendIntMessage("Worm.Respawn", 3)  
        
    lib_SetupWorm(8, "CPU_Worm4") 
    SendIntMessage("Worm.Respawn", 8)   
    
    lib_SetupWorm(9, "CPU_Worm5") 
    SendIntMessage("Worm.Respawn", 9)   
    
    lib_SetupWorm(10, "CPU_Worm6") 
    SendIntMessage("Worm.Respawn", 10)  
    
    DEATHMATCH = true
    ArmThemCPUWorms()
        
    
    end
    
end

function Worm_Died()
 
    local nDeadWorm = GetData("DeadWorm.Id")
    
    if playerwasspotted == true and DEATHMATCH == false then
    
        if nDeadWorm == 0 then
            AlreadyPlayed = true
            MissionFail()
            
        end
        
    end
    
end


function TurnEnded()

    TeamCount = lib_GetActiveAlliances()
    WhichTeam = lib_GetSurvivingTeamIndex()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
    if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
    
        MissionFail()
        
    else
        -- PRE-DEATHMATCH RULES
        if DEATHMATCH == false then
           -- PLAYER DEAD BEFORE DEATHMATCH
           if TeamCount == 0 then
               MissionFail()
            elseif BlowTheGate == true and AlreadyPlayed == false then
                -- play blow the gate reminder EFMV
                BlowTheGateEFMV()

            elseif DEATHMATCH == false and BlowTheGate == false and playerwasspotted == false then
                -- Play enemy looking around EFMV 
                EnemyLook()
                
            else
            
               StartTurn()
               
            end
        
        -- POST-DEATHMATCH RULES
        else
           -- ONLY PLAYER ALIVE
           if TeamCount == 1 and WhichTeam == 0 then

              MissionSuccess()
        
           -- PLAYER DEAD DURING DEATHMATCH
           elseif TeamCount == 1 and WhichTeam == 1 then

                MissionFail()

           -- PLAYER AND CPU BOTH ALIVE
           else
               StartTurn()      
    
           end
        end 
    end
        

end

function MissionFail()
  --SetData("GameToFrontEndDelayTime",1000)
  SendMessage("GameLogic.Mission.Failure")
    lib_DisplayFailureComment()
end


function MissionSuccess()
   lib_SetupWorm(11, "ProfWorm") 
   SendIntMessage("Worm.Respawn", 11)  
        
   lib_SetupWorm(12, "BoggyWorm") 
   SendIntMessage("Worm.Respawn", 12)   
    
   lib_SetupWorm(13, "BoggyMen1") 
   SendIntMessage("Worm.Respawn", 13)   
    
   lib_SetupWorm(14, "BoggyMen2") 
   SendIntMessage("Worm.Respawn", 14) 

    lock, worm = EditContainer("Worm.Data12")
    worm.ATT_Hat = "WXFE.A.Hat.Cowboy2"
    CloseContainer(lock)
    
    
    SendIntMessage("Worm.DieQuietly", 0)
    SendIntMessage("Worm.DieQuietly", 1)
    SendIntMessage("Worm.DieQuietly", 2)
    SendIntMessage("Worm.DieQuietly", 3)
    
    
   SetData("EFMV.GameOverMovie", "Outro")
   --lib_DisplaySuccessComment()

   SetData("WXD.StoryMovie", "Arabian")
   SendMessage("GameLogic.Mission.Success") 
end     



function DoOncePerTurnFunctions()
       
        if GateHasBeenBlownUp == false and CrateOnLandscape == false then
       
            local inventory = QueryContainer( lib_GetAllianceInventoryName(0) )
            
        
            if inventory.Dynamite == 0 then
        
            -- Display some text telling the player to stop been a tard and blow the gate up.
        
                lib_SpawnCrate("Dynamite_Crate")
                CrateOnLandscape = true
            
            end

        end
  
end

function Crate_Collected()

    CrateNumber = GetData("Crate.Index")
    
    if CrateNumber == 1 then
    
        lib_SpawnCrate("RedBull_Crate") 
            
    elseif CrateNumber == 3 then
    
        lib_SpawnCrate("Parachute_Crate") 
        
    elseif CrateNumber == 4 then
        
        lib_SpawnCrate("Dynamite_Crate") 
        
    elseif CrateNumber == 2 then
    
        if TriggeredOnce == false then
            lib_SpawnCrate("Target")
            BlowTheGate = true
            TriggeredOnce = true
        end
        
    end
    
--    if CrateNumber == 2 then
--    
--        CrateOnLandscape = false
--        
--    end
    
end


function BlowTheGateEFMV()

    if GateHasBeenBlownUp == false then
        SetData("EFMV.MovieName", "DestroyGate")
        SendMessage("EFMV.Play")
--~         BlowTheGate = false
--~         AlreadyPlayed = true
    end
    
end

function EnemyLook()

    if WhichEnemy == false then
        SetData("EFMV.MovieName", "EnemyLook1")
        SendMessage("EFMV.Play")
        WhichEnemy = true
    else 
        SetData("EFMV.MovieName", "EnemyLook2")
        SendMessage("EFMV.Play")
        WhichEnemy = false
    end

end







