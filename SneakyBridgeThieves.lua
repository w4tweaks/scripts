-- Mission Sneaky Bridge Theives 


function Initialise()						

    SetData("TurnTime", 45000)
    SetData("Land.Indestructable", 1)

    lock, scheme = EditContainer("GM.SchemeData")
        scheme.HelpPanelDelay = 0          
    CloseContainer(lock)
    
	lib_SpawnCrate("Crate_1") 
    lib_SpawnCrate("Crate_2") 
    lib_SpawnCrate("Crate_3") 
    lib_SpawnCrate("Crate_4") 
    lib_SpawnCrate("Crate_5") 
    lib_SpawnCrate("Crate_6") 
    lib_SpawnCrate("Crate_7") 
    lib_SpawnCrate("Crate_8") 
    lib_SpawnCrate("Crate_9") 
    lib_SpawnCrate("Crate_10")
    lib_SpawnCrate("Crate_11")
    lib_SpawnCrate("CrateGirder")
    lib_SpawnCrate("CrateParachute")
    
    Emitter1 = lib_CreateEmitter("WXP_CollectableItem", "Crate1") 
    Emitter2 = lib_CreateEmitter("WXP_CollectableItem", "Crate2") 
    Emitter3 = lib_CreateEmitter("WXP_CollectableItem", "Crate3") 
    Emitter4 = lib_CreateEmitter("WXP_CollectableItem", "Crate4") 
    Emitter5 = lib_CreateEmitter("WXP_CollectableItem", "Crate5") 
    Emitter6 = lib_CreateEmitter("WXP_CollectableItem", "Crate6") 
    Emitter7 = lib_CreateEmitter("WXP_CollectableItem", "Crate7") 
    Emitter8 = lib_CreateEmitter("WXP_CollectableItem", "Crate8") 
    
    lib_SetupTeam(0, "BridgeBuilderTeam")
    --lib_SetupTeam(1, "BridgeKeeperTeam")
    lib_SetupTeam(1, "CPUTeam")
    lib_SetupWorm(0, "PlayerWorm_1")  
    lib_SetupWorm(1, "PlayerWorm_2")  
--    lib_SetupWorm(2, "CPUWorm_1")  
--    lib_SetupWorm(3, "CPUWorm_2")  
--    lib_SetupWorm(4, "CPUWorm_3")  
--    lib_SetupWorm(5, "CPUWorm_4")  
    
    WormAILevel = "AIParams.CPU1"
    CopyContainer(WormAILevel, "AIParams.Worm02")
    CopyContainer(WormAILevel, "AIParams.Worm03")
    CopyContainer(WormAILevel, "AIParams.Worm04")
    
    local lock, team = EditContainer("Team.Data02")
    

    team.ATT_Hat = "WXFE.A.Hat.Helmet"
    team.ATT_Glasses = ""
    team.ATT_Gloves = ""
    team.ATT_Tash = ""
    
    CloseContainer(lock)
    
    local lock, worm = EditContainer("Worm.Data00")
    worm.WeaponIndex = 37 -- the code for the rope
    CloseContainer(lock)

    SendMessage("WormManager.Reinitialise")

    lib_SetupTeamInventory(0, "BuilderInventory")
    lib_SetupTeamInventory(1, "CPUInventory")
   
	g_nNumberofCPUWormsThatAreDead = 0
    
    PlayerWormDead = 0
    CratesCollected = 0
    DontPlayAgain = false
        
    PlayIntroMovie()
    
	--StartFirstTurn()

end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "SneakyBridgeIntro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()

  local WhichMovie = GetData("EFMV.MovieName")
  
    if WhichMovie == "SneakyBridgeIntro" then
    
        StartFirstTurn()
        
    elseif WhichMovie == "Midtro" then
    
        StartTurn()
        
    end
    
end
 
function Worm_Died()
 
    local nDeadWorm = GetData("DeadWorm.Id")
    
    if nDeadWorm == 0 or nDeadWorm == 1 then -- checks to see if any of the players worms have died, if they have then game over.
        PlayerWormDead = PlayerWormDead + 1
        
    end
    
    if PlayerWormDead == 2 then
    
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    end

end

function TurnEnded() -- this will not get called once "success" or "failure" has been sent

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
            --SetData("GameToFrontEndDelayTime",1000)
            
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        
        else
        
            if TeamCount == 0 then
                SendMessage("GameLogic.Mission.Failure")
                lib_DisplayFailureComment()
            end

            local lock, worm = EditContainer("Worm.Data00")
            worm.WeaponIndex = 37 -- the code for the rope
            CloseContainer(lock)

            StartTurn()
            
        end
	  
end

function Crate_Collected()

    
    IndexNumber = GetData("Crate.Index")


    if IndexNumber <= 8 then
    
    
        CratesCollected = CratesCollected + 1

    
        if IndexNumber == 1 then

            lib_DeleteEmitterImmediate(Emitter1)
        
        elseif IndexNumber == 2 then

            lib_DeleteEmitterImmediate(Emitter2)
        
        elseif IndexNumber == 3 then

            lib_DeleteEmitterImmediate(Emitter3)
        
        elseif IndexNumber == 4 then

            lib_DeleteEmitterImmediate(Emitter4)
            
        elseif IndexNumber == 5 then

            lib_DeleteEmitterImmediate(Emitter5)
            
        elseif IndexNumber == 6 then

            lib_DeleteEmitterImmediate(Emitter6)
            
        elseif IndexNumber == 7 then

            lib_DeleteEmitterImmediate(Emitter7)
            
        elseif IndexNumber == 8 then

            lib_DeleteEmitterImmediate(Emitter8)
            
        end
        
        if CratesCollected < 8 then
            DisplayCollectComment()
        end
    end
    
    if CratesCollected == 3 then
    
        if DontPlayAgain == false then
    
            SetData("EFMV.MovieName", "Midtro")
            SendMessage("EFMV.Play")
        
            lib_SetupWorm(2, "CPUWorm_1") 
            SendIntMessage("Worm.Respawn", 2)   
    
            lib_SetupWorm(3, "CPUWorm_2") 
            SendIntMessage("Worm.Respawn", 3)   
    
            lib_SetupWorm(4, "CPUWorm_3") 
            SendIntMessage("Worm.Respawn", 4)  
           
           DontPlayAgain = true
	   EndTurn()
           
        end
        
    end
    
    if CratesCollected == 8 then
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
    end
        
end


function DisplayCollectComment()
   local Comment = { "C.Generic.Good.1", "C.Generic.Good.2", "C.Generic.Good.3" }
   local MyRand = lib_GetRandom(1,3)
   SendMessage("Commentary.Clear")  
   SetData("CommentaryPanel.Comment", Comment[MyRand] )
   SetData("CommentaryPanel.Delay", 3000)
   SendMessage("CommentaryPanel.TimedText")
end