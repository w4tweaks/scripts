function Initialise()
	
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 30000)
	
	-- team setup; worm setup and inventories cloned from databank
	lib_SetupTeam(0, "Human Team")
	lib_SetupTeam(1, "AI Team")
	lib_SetupWorm(0, "Player")
	lib_SetupWorm(1, "CPU")

	lock, AIParams = EditContainer("AIParams.Worm01") 
	CloseContainer(lock)

	SendMessage("WormManager.Reinitialise")
	
	
	lib_SetupTeamInventory(0, "Inv Human")
	lib_SetupTeamInventory(1, "Inv CPU")
	

--    Stage2Done = false
    
    PlayIntroMovie()

end

function PlayIntroMovie()

    SetData("EFMV.MovieName", "TinCanOuttake")
    SendMessage("EFMV.Play")
    
end

function EFMV_Terminated()
        SendMessage("GameLogic.Mission.Success")
end
