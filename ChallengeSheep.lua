function Initialise()
	
	SetData("Land.Indestructable", 1)
	SetData("TurnTime", 0)
    SetData("RoundTime",0)
	SetData("HotSeatTime", 0) 
    SetData("RetreatTime", 0) 
    SetData("DefaultRetreatTime",0)
    SetData("PostActivityTime",0)
    SetData("HUD.Clock.DisplayTenths", 1)
    
	lib_SetupTeam(0, "HumanTeam")
	lib_SetupWorm(0, "Player")
	lib_SetupTeamInventory(0, "Inv_Human")
	
	
	SendMessage("WormManager.Reinitialise")
	
	SetData("Trigger.Visibility", 1)
		
    g_nNextTarget = 1
    ExtraFuel = 10000
    TotalTargetNumber = 5
    SetData("HUD.Counter.Active", 1)
    SetData("HUD.Counter.Value", 5)
    SpawnNextTarget()
   -- StartFirstTurn()
   PlayIntroMovie()
    
    SendMessage("Commentary.NoDefault")
    
    lock, weapon = EditContainer("kWeaponSuperSheep")
    weapon.LifeTime = -1
    CloseContainer(lock)
   
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()
    StartFirstTurn()
end

function Crate_Collected()


    TotalTargetNumber = TotalTargetNumber - 1 

    SetData("HUD.Counter.Value", TotalTargetNumber)
    
    TextToDisplay = {"C.Generic.Good.1","C.Generic.Good.2","C.Generic.Good.3","C.Generic.Time.1"}
        local myRandomInteger = lib_GetRandom(1, 4)
        
        lib_Comment(TextToDisplay[myRandomInteger])
    
    SpawnNextTarget()

end


function SpawnNextTarget()

    if g_nNextTarget == 6 then
    
        lib_DisplaySuccessComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Success")
        
    else
    
    -- target cycle 
    local Crate = {"Targ1", "Targ2", "Targ3", "Targ4", "Targ5", "Targ6", 
    				"Targ7", "Targ8"}
        lib_SpawnCrate(Crate[g_nNextTarget])
        g_nNextTarget = g_nNextTarget + 1
        
    log("g_nNextTarget = ", g_nNextTarget)
    
    end

end


function TurnEnded()
        StartTurn()
    
end

function Worm_Died()

    deadWorm = GetData("DeadWorm.Id")
    
    if deadWorm == 0 then
    
        lib_DisplayFailureComment()
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Challenge.Failure")
    end
end


