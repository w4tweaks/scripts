

function Initialise()


    -- These details are copied from the "databank" for this mission
    -- The players "Mission Team" name and so on will also be TIC
    lib_SetupTeam(0, "team0")
    lib_SetupTeam(1, "team1")
    
    lib_SetupWorm(0, "worm0")
    lib_SetupWorm(1, "worm1")

    -- Again cloned from the one in the databank
    lib_SetupTeamInventory(0, "inventory")
    lib_SetupTeamInventory(1, "inventory")

    SendMessage("WormManager.Reinitialise")

    lib_SpawnCrate("mycrate")

    SetData("TurnTime", 0)

    SetData("RoundTime", -1)
--    SetData("HUD.Clock.DisplayTenths", 1)

    --g_BoxId = "Tut1.EFMV.Dialogue1"
    --lib_CreateBriefingBox(g_BoxId)
    
    g_CratesCollected = 0
    SetData("HUD.Counter.Active", 1)
    SetData("HUD.Counter.Percent",1)
    SetData("HUD.Counter.Value", 100)

    SetData("HUD.Energy.Visible.Team00", 0)
    
    SendMessage("GameLogic.CreateRandMineFactory")


    SetData("EFMV.MovieName", "movie2")
    SendMessage("EFMV.Play")

end


function Land_NewShape()
    local land_remaining = 0
    local percent_remaining = 100*((land_remaining - 100)/234)
    SetData("HUD.Counter.Value", percent_remaining)
end


function Game_BriefingDialogOkPressed()
    --if g_BoxId == "Tut1.EFMV.Dialogue1" then
    --    StartFirstTurn()
    --end
end

function TurnEnded()

    StartTurn()
--lib_DeathmatchMissionTurnEnded()
--    SetData("EFMV.MovieName", "movie2")
--    SendMessage("EFMV.Play")

end


function EFMV_Terminated()
    StartTurn()
end

function Crate_Collected()
    lib_SpawnCrate("mycrate")
    SendMessage("Comment.SuddenDeath")
    g_CratesCollected = g_CratesCollected + 1

    SetData("TurnTime", 30000)

    SetData("RoundTime", 3000000)

--    SendIntMessage("WXWormManager.UnspawnWorm", 1)

end

function Weapon_Selected()
end

function GameLogic_WeaponPanelOpened()
    --lib_CreateBriefingBox("Tut1.EFMV.Dialogue1")
    --SendMessage("Comment.SuddenDeath")
end

function GameLogic_WeaponPanelClosed()

end


function Worm_Died()

    wormId = GetData("DeadWorm.Id")
    if wormId == 0 then
        lib_SetupWorm(0, "worm0")
        SendIntMessage("Worm.Respawn",0)
    end
end

function DoOncePerTurnFunctions()
   SendMessage("GameLogic.StartMineFactory")
end
