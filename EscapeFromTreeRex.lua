

function Initialise()	

--SetData("RoundTime", 3000000)
    -- Initialise the global variables
    g_nDeadEnemies = 0
    g_sNextCrate = "CrateSpawn1"
    g_nCratesOnLand = 0
    g_nStage = 1
        
    EndTriggerCollected = false
    PlayTheCaveMovie = false

    SetData("Land.Indestructable", 1)
	
    SpawnNextCrate(0) 

    ---	Worms and teams setup here
    lib_SetupTeam(0, "PlayerTeam")
    lib_SetupTeam(1, "cpu")

	lib_SetupWorm(0, "Worm1")  -- Players worm
    lib_SetupWorm(1, "Worm2")  -- Cpu worm
	lib_SetupWorm(2, "Worm3")  -- Cpu Worm
    SendMessage("WormManager.Reinitialise")

    WormAILevel = "AIParams.CPU3"        
    CopyContainer(WormAILevel, "AIParams.Worm01")
    CopyContainer(WormAILevel, "AIParams.Worm02") 
    CopyContainer(WormAILevel, "AIParams.Worm03")
    CopyContainer(WormAILevel, "AIParams.Worm04") 
    CopyContainer(WormAILevel, "AIParams.Worm05")       
    
    
	lib_SetupTeamInventory(1, "cpInventory")
    lib_SetupTeamInventory(0, "PlayerInventory")
    
    SetData("Trigger.Visibility", 0)
    lib_SpawnTrigger("EndTrigger")
    
    local EasterEgg= QueryContainer( "Lock.EasterEgg.4")
    if EasterEgg.State ~= "Unlocked"  then
	lib_SpawnTrigger("EasterTrigger")
    end
   
    
    PlayIntroMovie()

end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

--end of turn check for the next crate

function Worm_Died()	
		
    local WormId = GetData("DeadWorm.Id")
    if WormId == 0 then  
        endmissionfail() -- the players worm died
    else
	    g_nDeadEnemies = g_nDeadEnemies + 1
        
        if g_nDeadEnemies == 5 then
--            lib_Comment("M.Jur.Escape.MID.2") -- Get to the end of the cave

            PlayTheCaveMovie = true
            
        end
    end
        
end


function DoOncePerTurnFunctions()

    if PlayTheCaveMovie == true and EndTriggerCollected == false then
    
        PlayTheCaveMovie = false
    
        SetData("EFMV.MovieName", "MidtroCave")
        SendMessage("EFMV.Play")
    
    end


    -- Decide whether to spawn a new crate

    -- 1st stage: grenades
    if g_nStage == 1 then          
        local inventory = QueryContainer( lib_GetAllianceInventoryName(0) )
        if g_nDeadEnemies < 2 and inventory.Bazooka == 0 and g_nCratesOnLand == 0 then
            SpawnNextCrate(1)
        end
    -- 2nd stage: mines
    else 
        local inventory = QueryContainer( lib_GetAllianceInventoryName(0) )
        if inventory.BaseballBat == 0 and g_nCratesOnLand == 0 then
            SpawnNextCrate(1)
        end
    end
  
end


function TurnEnded()

    -- If the player dies we never get to this function
    -- (we ended in WormDied())
            
    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
            --SetData("GameToFrontEndDelayTime",1000)

            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
            
        else
	
	
	    if EndTriggerCollected == true then
	    
		    -- The end trigger in the cave has been collected this
			-- turn, check for success
		if g_nDeadEnemies == 5 then
    	    
		    endmissionsuccess()
		    return	-- spp is this necessary to avoid the StartTurn below
		end
    
		-- to get this far the mission can't have been won yet
                lib_SpawnTrigger("EndTrigger")
                EndTriggerCollected = false
        
            end
	
	
	
            if g_nDeadEnemies == 2 and g_nStage == 1 then
                g_nStage = 2
                StartStage2EFMV() -- the next turn will be started after this is finished
            else
                StartTurn()
            end           
            
        end

end


function SpawnNextCrate(camera)

    -- for each stage alternate between 2 crates

     lib_SetupCrate(g_sNextCrate)
     SetData("Crate.TrackCam", camera)
     SendMessage("GameLogic.CreateCrate")
  
     g_nCratesOnLand = 1

     -- the first two are the grenade crates
     if g_sNextCrate == "CrateSpawn1" then
        g_sNextCrate = "CrateSpawn2"
     elseif g_sNextCrate == "CrateSpawn2" then
        g_sNextCrate = "CrateSpawn1" 

     -- these 2 are the mine crates
     elseif g_sNextCrate == "CrateSpawn3" then
        g_sNextCrate = "CrateSpawn4" 
     elseif g_sNextCrate == "CrateSpawn4" then
        g_sNextCrate = "CrateSpawn3"
     end 
end


function Crate_Collected()
   g_nCratesOnLand = 0
end


function StartStage2EFMV()


    SetData("EFMV.MovieName", "Midtro")
    SendMessage("EFMV.Play")


    -- these will be able to be spawned as part of the movie
	SetData("Wind.Speed", 0) -- no wind so we know where they will land
    StartTimer("SpawnWorm3", 1000)
	StartTimer("SpawnWorm4", 1500)
	StartTimer("SpawnWorm5", 2000)
    
end


function EFMV_Terminated()
   local Movie = GetData("EFMV.MovieName")
   if Movie == "Intro" then
   
       -- lib_CreateEmitter("WXPS_woodbreak", "Crunch")
      StartFirstTurn()
   elseif Movie == "Midtro" then
      g_sNextCrate = "CrateSpawn3"
      SpawnNextCrate(0)
      StartTurn()
      
      
    elseif Movie == "MidtroCave" then
    	
        StartTurn()
 
   end
end

	
function SpawnWorm3()
    lib_SetupWorm(3, "Worm1Spawn") 
    SendIntMessage("Worm.Respawn", 3)   
end


function SpawnWorm4()
    lib_SetupWorm(4, "Worm2Spawn") 
    SendIntMessage("Worm.Respawn", 4)    
end


function SpawnWorm5()
    lib_SetupWorm(5, "Worm3Spawn") 
	SendIntMessage("Worm.Respawn", 5)
end


function endmissionsuccess()
    SetData("EFMV.GameOverMovie", "Outro")
    SendMessage("GameLogic.Mission.Success") 
    lib_DisplaySuccessComment()
end


function endmissionfail()
    SendMessage("GameLogic.Mission.Failure") 
    lib_DisplayFailureComment()   
end

function Trigger_Collected()
	
        EndTriggerCollected = true        
	
	if g_nDeadEnemies < 5 then
       	    
		lib_Comment("M.Jur.Escape.MID.1")  -- Kill all the worms so they don't follow you
	
	elseif g_nDeadEnemies == 5 then
	
		EndTurn()
        end
end

function Trigger_Destroyed()

    TriggerIndex = GetData("Trigger.Index")

    if TriggerIndex == 7 then -- EasterEgg
    
    
      SendStringMessage("WXMsg.EasterEggFound", "Lock.EasterEgg.4") -- unlock easter egg
        SetData("CommentaryPanel.Delay", 4000) -- 4 seconds
        SetData("CommentaryPanel.Comment", "M.Easter.1") -- You have found a easter egg, lucky you!
        SendMessage("CommentaryPanel.TimedText")
        lib_CreateEmitter("WXPS_Roar","Roar" )
        
    end
    
end




