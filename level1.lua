-- Example 1: Simple Deathmatch

function Initialise()

	-- These details are copied from the "databank" for this mission
    -- The players "Mission Team" name and so on will also be TIC
    lib_SetupTeam(0, "myteam")
    lib_SetupTeam(1, "AIteam")
--    lib_SetupTeam(2, "team02")
--    lib_SetupTeam(3, "team03")
    
    lib_SetupWorm(0, "myworm")
   lib_SetupWorm(1, "AIworm")
 --   lib_SetupWorm(2, "worm02")
 --   lib_SetupWorm(3, "worm03")

    -- Again cloned from the one in the databank
    lib_SetupTeamInventory(0, "myinventory")
    lib_SetupTeamInventory(1, "myinventory")
 --   lib_SetupTeamInventory(2, "myinventory")
 --   lib_SetupTeamInventory(3, "myinventory")

    SendMessage("WormManager.Reinitialise")

    -- The databank contains details of this crate

    SetData("TurnTime", 0)

    lib_SpawnCrate("mycrate")

    StartFirstTurn()

end


function TurnEnded()
    
    -- Standard Deathmatch mission rule check and game ending
    -- ie. last man standing, display end of game comment and set awards
    lib_DeathmatchMissionTurnEnded()

end

function Crate_Collected()
    lib_SpawnCrate("mycrate")
end
