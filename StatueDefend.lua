function Initialise()

    lib_SetupMultiplayer()

    SendMessage("WormManager.Reinitialise")
    lib_SetupMinesAndOildrums() -- do this after worms are set up to get correct worm collision

    g_TeamStatues = { -1,-1,-1,-1 }

    for team = 0,3 do
        local TeamContainer = QueryContainer(lib_GetTeamContainerName(team))
        if TeamContainer.Active then
           -- remap the spawn area for this team to be the centre of the map from now on
           SetData("SpawnVolume.CopyFrom", 4)
           SendIntMessage("SpawnVolume.Remap", team)

           -- spawn in the 3 team statues
           SendIntMessage("GameLogic.CreateTeamStatues", team) -- much easier to randomise this in C++
           g_TeamStatues[team+1] = 3
       end
   end

   StartFirstTurn()

end

function Crate_Destroyed() 

    -- if this is the last crate kill the team off
    local team = GetData("Crate.Index")
    if team ~= -1 then -- ignore random crate drops
       g_TeamStatues[team+1] = g_TeamStatues[team+1] - 1
       SetData( "Turn.Boring", 0 )
       SetData( "Turn.MaxDamage", 1 )
    end      
end

function Crate_Sunk() 

    -- if this is the last crate kill the team off
    local team = GetData("Crate.Index")
    if team ~= -1 then -- ignore random crate drops
       g_TeamStatues[team+1] = g_TeamStatues[team+1] - 1
       SetData( "Turn.Boring", 0 )
       SetData( "Turn.MaxDamage", 1 )
    end      
end


function Worm_Died()
    -- spawn a replacement if the team is still in play
    local WormIndex = GetData("DeadWorm.Id")
    local WormContainer = lib_QueryWormContainer(WormIndex)
    local team = WormContainer.TeamIndex
    if g_TeamStatues[team+1] > 0 then
        local scheme = QueryContainer("GM.SchemeData")
        local lock, EditWorm = EditContainer(lib_GetWormContainerName(WormIndex))
        EditWorm.Energy = scheme.WormHealth
        EditWorm.Active = true
        CloseContainer(lock)
        SendIntMessage("Worm.Respawn", WormIndex)
    end
end

function DoOncePerTurnFunctions()
    local nTeamsRemaining = 0
    for team = 0,3 do
       if g_TeamStatues[team+1] > 0 then
          nTeamsRemaining = nTeamsRemaining + 1
       elseif g_TeamStatues[team+1] == 0 then
          g_TeamStatues[team+1] = -1
          local WormIndex
          for WormIndex=0,15 do
             local worm = QueryContainer(lib_GetWormContainerName(WormIndex))
             if worm.TeamIndex == team then
                SendIntMessage("WXWormManager.UnspawnWorm", WormIndex)
             end
          end
       end     
    end
    if nTeamsRemaining > 1 then
       SendMessage("GameLogic.DropRandomCrate")
    end
end

function TurnEnded()
   local RoundTimeRemaining = GetData("RoundTimeRemaining")
   if RoundTimeRemaining == 0 then
      SendMessage("GameLogic.Draw")
   else
      CheckOneTeamVictory()      
   end
end

function TurnStarted()
   UpdateCounter()
end


function UpdateCounter()
    local Data = { "HUD.Counter.Team00", "HUD.Counter.Team01", "HUD.Counter.Team02", "HUD.Counter.Team03" }
    for team = 0,3 do
       if g_TeamStatues[team+1] ~= -1 then
          SetData(Data[team+1], g_TeamStatues[team+1])
       end
    end
end
