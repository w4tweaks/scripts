-- Mystery Crates Testing
function Initialise()
    -- Sets up mine fuse time and probability
	SetData("Mine.DudProbability", 0)
	SetData("Mine.MinFuse", 3000)
	SetData("Mine.MaxFuse", 3000)

    -- Create mines and oildrums 
    SendMessage("GameLogic.PlaceObjects")

    --spawn crates
    --lib_SpawnCrate("Buffalo")
    
    SetupWormsAndTeams()
    SetupInventories()

    SetData("Camera.StartOfTurnCamera", "Default")
    StartFirstTurn()
end

function SetupWormsAndTeams()
-- Activate Team 0
    lock, team = EditContainer("Team.Data00") 
    team.Active = true
    team.Name = "Team 1"
    team.TeamColour = 0
-- team.IsAIControlled = true
    CloseContainer(lock) -- must close the container ASAP

-- Activate Team 1
    lock, team = EditContainer("Team.Data01") 
    team.Active = true
    team.Name = "Team 2"
    team.TeamColour = 1
-- team.IsAIControlled = true
    CloseContainer(lock)

-- Activate Team 2
    lock, team = EditContainer("Team.Data02") 
    team.Active = true
    team.Name = "Team 3"
    team.TeamColour = 2
-- team.IsAIControlled = true
    CloseContainer(lock)

-- Worm 0, Team 0
    lock, worm = EditContainer("Worm.Data00") 
    worm.Active = true
    worm.Name = "T1,W1"
    worm.Energy = 100
    worm.WeaponFuse = 3
    worm.WeaponIsBounceMax = false
    worm.TeamIndex = 0
    worm.SfxBankName = "Pirate"
    worm.Spawn = "spawn"
    CloseContainer(lock) 

-- Worm 4, Team 1
    CopyContainer("Worm.Data00", "Worm.Data01")
    lock, worm = EditContainer("Worm.Data01")
    worm.Energy = 80 
    worm.Name = "T2,W1"
    worm.TeamIndex = 1
    worm.SfxBankName = "Biggles"
    worm.Spawn = "spawn"
    CloseContainer(lock)

-- Worm 8, Team 1
    CopyContainer("Worm.Data00", "Worm.Data02")
    lock, worm = EditContainer("Worm.Data02")
    worm.Energy = 80 
    worm.Name = "T3,W1"
    worm.TeamIndex = 2
    worm.SfxBankName = "Dumbass"
    worm.Spawn = "spawn"
    CloseContainer(lock)

    SendMessage("WormManager.Reinitialise")
end

function SetupInventories()
-- sets up a default container and adds our selection to it
    lock, inventory = EditContainer("Inventory.Team.Default") 
    inventory.Bazooka = 3
    
    CloseContainer(lock) -- must close the container ASAP

    lock, weapon = EditContainer("kWeaponSuperSheep")
    weapon.LifeTime = -1
    CloseContainer(lock)

-- Copies this selection into each worm
   CopyContainer("Inventory.Team.Default", "Inventory.Alliance00")
   CopyContainer("Inventory.Team.Default", "Inventory.Alliance01")
end