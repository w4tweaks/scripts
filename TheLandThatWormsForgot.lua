
--The Land That Worms Forgot

function Initialise()

    SetData("HotSeatTime", 5000)
	SetData("TurnTime", 30000)
    SetData("RoundTime", 3000000)

    ---	Worms and teams setup here
    lib_SetupTeam(0, "Human_Team")
    lib_SetupTeam(1, "CPU_Team")
    --lib_SetupTeam(2, "Human_Team2")

	lib_SetupWorm(0, "Human_Worm")  -- Players worm
    lib_SetupWorm(1, "Human_Worm2")
    lib_SetupWorm(2, "Human_Worm3")
    lib_SetupWorm(3, "Human_Worm4")
    lib_SetupWorm(4, "CPU_Worm1")  -- Players worm
    lib_SetupWorm(5, "CPU_Worm2")
    lib_SetupWorm(6, "CPU_Worm3")
    lib_SetupWorm(7, "CPU_Worm4")
    lib_SetupWorm(8, "CPU_Worm5")
    lib_SetupWorm(9, "CPU_Worm6")
    lib_SetupWorm(10, "CPU_Worm7")
    lib_SetupWorm(11, "CPU_Worm8")
    SendMessage("WormManager.Reinitialise")

    WormAILevel = "AIParams.CPU2"        
    CopyContainer(WormAILevel, "AIParams.Worm04") 
    CopyContainer(WormAILevel, "AIParams.Worm05")   
    CopyContainer(WormAILevel, "AIParams.Worm06")
    CopyContainer(WormAILevel, "AIParams.Worm07")     
    CopyContainer(WormAILevel, "AIParams.Worm08") 
    CopyContainer(WormAILevel, "AIParams.Worm09")
    CopyContainer(WormAILevel, "AIParams.Worm10")
    CopyContainer(WormAILevel, "AIParams.Worm11")

    lib_SetupTeamInventory(1, "CPU_Inventory")
    lib_SetupTeamInventory(0, "Human_Inventory")
    
    lib_SpawnCrate("Crate_Shotgun") 
    lib_SpawnCrate("Crate_Sniper") 
    --lib_SpawnCrate("Crate_PoisonArrow") 
    lib_SpawnCrate("Crate_Donkey") 
    lib_SpawnCrate("Crate_Health") 

    SetData("Trigger.Visibility", 0)
    lib_SpawnTrigger("Return_Trigger1")
    
--    lock, AIParams = EditContainer("AIParams.Worm04")

--    AIParams.ForbidShotsWhenMightAffectAITrigger = 1.0 

--    CloseContainer(lock)
--    
--    lock, AIParams = EditContainer("AIParams.Worm05")

--    AIParams.ForbidShotsWhenMightAffectAITrigger = 1.0 

--    CloseContainer(lock)
--    
--    lock, AIParams = EditContainer("AIParams.Worm06")

--    AIParams.ForbidShotsWhenMightAffectAITrigger = 1.0 

--    CloseContainer(lock)
--    
--    lock, AIParams = EditContainer("AIParams.Worm07")

--    AIParams.ForbidShotsWhenMightAffectAITrigger = 1.0 

--    CloseContainer(lock)
--    
--    lock, AIParams = EditContainer("AIParams.Worm08")

--    AIParams.ForbidShotsWhenMightAffectAITrigger = 1.0 

--    CloseContainer(lock)
--    
--    lock, AIParams = EditContainer("AIParams.Worm09")

--    AIParams.ForbidShotsWhenMightAffectAITrigger = 1.0 

--    CloseContainer(lock)
--    
--    lock, AIParams = EditContainer("AIParams.Worm10")

--    AIParams.ForbidShotsWhenMightAffectAITrigger = 1.0 

--    CloseContainer(lock)
--    
--    lock, AIParams = EditContainer("AIParams.Worm11")

--    AIParams.ForbidShotsWhenMightAffectAITrigger = 1.0 

--    CloseContainer(lock)

    SpawnNewTriggerIfGameNotEnded = false
    EnemyWormsAreAllDead = 0
    PlayerHasLostAWorm = 0
    PlayKillAllWorms = true
    PlayFindWorm = true
    
    SelectRandomWind()
    PlayIntroMovie()
    
end

function PlayIntroMovie()
    SetData("EFMV.MovieName", "Intro")
    SendMessage("EFMV.Play")
end

function EFMV_Terminated()

    local WhichMovie = GetData("EFMV.MovieName")
    
    if WhichMovie == "Intro" then
        StartFirstTurn()
    
    else
        StartTurn()
    end
end

function Trigger_Collected()

    SpawnNewTriggerIfGameNotEnded = true

    if EnemyWormsAreAllDead == 8 then
        SetData("EFMV.GameOverMovie", "Outro")
        SendMessage("GameLogic.Mission.Success")
        lib_DisplaySuccessComment()
    else
    
        if PlayKillAllWorms == true then
            SetData("EFMV.MovieName", "KillEnemy")
            SendMessage("EFMV.Play")
            PlayKillAllWorms = false
        end
        
    end
    
end

--Success/Failure checks
function  TurnEnded()

    local RoundTimeRemaining = GetData("RoundTimeRemaining")
        
        if RoundTimeRemaining == 0 or RoundTimeRemaining < 0 then
         
            --SetData("GameToFrontEndDelayTime",1000)
            SendMessage("GameLogic.Mission.Failure")
            lib_DisplayFailureComment()
        
        else
        

            if SpawnNewTriggerIfGameNotEnded == true then

                lib_SpawnTrigger("Return_Trigger1")
                SpawnNewTriggerIfGameNotEnded = false
        
            end
    
            TeamCount = lib_GetActiveAlliances()
            WhichTeam = lib_GetSurvivingTeamIndex()
	
            if TeamCount == 0 then
                
                SendMessage("GameLogic.Mission.Failure")
                lib_DisplayFailureComment()
            elseif TeamCount == 1 and WhichTeam == 0 then
                
                SendMessage("GameLogic.Mission.Failure")
                lib_DisplayFailureComment()

            end
    
            if EnemyWormsAreAllDead == 8 then

                if PlayFindWorm == true then
                    SetData("EFMV.MovieName", "FindWorm")
                    SendMessage("EFMV.Play")
                    PlayFindWorm = false
                else
                    StartTurn()
                end
--~                 
            else
    
                StartTurn()
            
            end
        end
                
end

function Worm_Died()	
		
    deadWorm = GetData("DeadWorm.Id")
    
    if deadWorm > 3 then
    
        EnemyWormsAreAllDead = EnemyWormsAreAllDead + 1
        
    end
    
    if deadWorm == 3 then
    
        
        SendMessage("GameLogic.Mission.Failure")
        lib_Comment("M.Jur.Land.NEW11.K3") -- Dont kill him!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        
    end
    
    if deadWorm == 0 or deadWorm == 1 or deadWorm == 2 then
    
        PlayerHasLostAWorm = PlayerHasLostAWorm + 1
        
    end
    
    if PlayerHasLostAWorm == 3 then
    
        
        SendMessage("GameLogic.Mission.Failure")
        lib_DisplayFailureComment()
        
    end
    
end


